// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

let host = window.location.host;
let APIURL;

if (host.indexOf('7410') != -1) {
  APIURL = 'http://localhost/ringthebell/index.php/web/';
} else {
  APIURL = 'http://ringthebell.in/api/index.php/web/';
}


export const environment = {
  production: false,
  apiURL: APIURL,
  headers: { 'Authorization': 'Bearer VDVZS0IzdlVGRGNZZFJPSG5vSjhsNVpsQ2VFNTF1Nk5OeWlsdkVPcmVkS3VYZ2c4TkVjN1NlUFJmc2phVm1iYkRMZ09zMUo3dXZJMnpsODBKQ2wxRDlEejlnR0tCZWtOZWNUdTBpaXo1QURGTmtGUElnaGMwK2p4Q21FZ0k2eVM1YUI0L09NcXdNTkF0NGpOaFZ6N2FBVXJqR1pZTjZ5ZHljeDJ1U05DY3dRPQ==' }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

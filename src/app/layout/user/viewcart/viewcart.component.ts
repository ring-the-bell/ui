import { Component, OnInit, TemplateRef } from '@angular/core';
import { HeaderService } from 'src/app/common/header/header.service';

import { ShopService } from '../shop/shop.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-viewcart',
  templateUrl: './viewcart.component.html',
  styleUrls: ['./viewcart.component.scss']
})
export class ViewcartComponent implements OnInit {
  modalRef: BsModalRef;
  viewCartList;
  viewCartAmount = 0;
  loader = false;
  selectedItem;
  action;

  public viewCartModalConfig = {
    animated: true,
    keyboard: false,
    backdrop: true,
    ignoreBackdropClick: true,
    class: ''
  };

  constructor(private headerService: HeaderService, private shopService: ShopService, private router: Router, private authService: AuthService, private modalService: BsModalService, private toastr: ToastrService, private cookieService: CookieService) { }

  ngOnInit() {
    this.loader = true;
    if (this.authService.isLoggedIn()) {
      let role = JSON.parse(this.authService.getUserInfo());
      if (role) {
        if (role.userType != 'shopper') {
          this.router.navigate(['/admin']);
        } else {
          this.getViewCartList();
        }
      } else {
        this.getViewCartList();
      }
    } else {
      this.getViewCartList();      
    }
  }

  /**
   * TODO: comment getViewCartList
   * @description Gets view cart list
   * @author (Siva Sankar)
   */
  getViewCartList() {
    if (this.authService.isLoggedIn()) {
      this.getUserItems();
    } else {
      this.authService.logout();
    }
  }

  /**
   * TODO: comment getUserItems
   * @description Gets user items
   * @author (Siva Sankar)
   */
  getUserItems() {
    let url = 'cart/items/';
    this.headerService.getCartItems(url).subscribe(
      resp => {
        this.getItems(resp);
      },error => {
        console.log(error);
        this.loader = false;
      }
    );
  }

  /**
   * TODO: comment getItems
   * @description Gets items
   * @author (Siva Sankar)
   * @param resp 
   */
  getItems(resp) {
    if (resp && resp['data']) {  
      this.viewCartList = resp['data'];
      this.headerService.updateCartItems.next('Update Cart Items');
      this.getCartAmount();          
    } else {
      this.viewCartList = [];
      this.viewCartAmount = 0;
      this.headerService.updateCartItems.next('Update Cart Items');
    }
    this.loader = false;
  }

  /**
   * TODO: comment getCartAmount
   * @description Gets cart amount
   * @author (Siva Sankar)
   */
  getCartAmount() {
    this.viewCartAmount = this.viewCartList.reduce(function(prev, cur) {
      return prev + (parseFloat(cur.productOfferPrice) * parseFloat(cur.productQuantity));
    }, 0);
  }

  /**
   * TODO: comment updateQty
   * @description Updates qty
   * @author (Siva Sankar)
   * @param prod 
   * @param action 
   */
  updateQty(prod, action) {
    this.loader = true;
    console.log(prod, action);
    if (prod && action) {
      if (action == 'minus') {
        if (parseFloat(prod.productQuantity) >= 1) {
          prod.productQuantity = parseFloat(prod.productQuantity) - 1;
        } else {
          prod.productQuantity = 0;
        }
      } else if (action == 'plus') {
        prod.productQuantity = parseFloat(prod.productQuantity) + 1;
      } else if (action == 'focus') {
        if (parseFloat(prod.productQuantity) < 0) {
          prod.productQuantity = 0;
        }
      } else if (action == 'delete') {
        prod.productQuantity = 0;
      }
    }
    // this.getCartAmount();
    this.updateViewCart(prod, action);
    // this.loader = false;
  }

  /**
   * TODO: comment updateViewCart
   * @description Updates view cart
   * @author (Siva Sankar)
   * @param item 
   * @param action 
   */
  updateViewCart(item, action) {
    console.log(item);
    let cartObj;
    if (this.authService.isLoggedIn()) {
      cartObj = {
        prodQty: parseFloat(item.productQuantity),
        productId: item.productId,
        storeId: item.storeId
      };
      this.updateUserCart(cartObj, action);
    } else {
      this.authService.logout();
    }   
  }

  /**
   * TODO: comment updateUserCart
   * @description Updates user cart
   * @author (Siva Sankar)
   * @param cartObj 
   * @param action 
   */
  updateUserCart(cartObj, action) {
    this.shopService.updateUserCart(cartObj).subscribe(
      resp => {
        if (action && action == 'delete')
          this.modalRef.hide();
        this.toastr.success(resp['message']);
        this.loader = false;
        this.getViewCartList();
        this.headerService.updateCartItems.next('Update Cart Items');

      },err => {
        if (action && action == 'delete')
          this.modalRef.hide();
        this.toastr.error(err.error.message);
        this.loader = false;
        this.getViewCartList();
        this.headerService.updateCartItems.next('Update Cart Items');
      }
    );
  }

  /**
   * TODO: comment openModal
   * @description Opens modal
   * @author (Siva Sankar)
   * @param item 
   * @param action 
   * @param template 
   */
  openModal(item, action, template: TemplateRef<any>) {
    this.selectedItem = null;
    this.action = null;
    this.selectedItem = item;
    this.action = action;
    this.viewCartModalConfig.class = "modal-dialog-centered checkout-auth-modal-cls";
    this.modalRef = this.modalService.show(template, this.viewCartModalConfig);
  }

  /**
   * TODO: comment goToStore
   * @description Go to store
   * @author (Siva Sankar)
   */
  goToStore() {
    let store = null;
    if (this.cookieService.get('currentStore'))
      store = JSON.parse(this.cookieService.get('currentStore'));
    if (store && store.storeId) {
      this.router.navigate(['/groceries/store', btoa(store.storeId)]);
    } else {
      this.router.navigate(['/groceries/home']);
    }
  }

  /**
   * TODO: comment goToCheckout
   * @description Go to checkout
   * @author (Siva Sankar)
   */
  goToCheckout() {
    this.router.navigate(['/groceries/checkout']);
  }

}

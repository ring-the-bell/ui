import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment storeShipOpts
   * @description Stores ship opts
   * @author (Siva Sankar)
   * @param storeId 
   * @returns  
   */
  storeShipOpts(storeId) {
    return this.httpClient.get(`${this.APIURL}store/shipping/${storeId}`).pipe(
      map(resp => resp)
    );
  }
  
  /**
   * TODO: comment pushOrder
   * @description Pushs order
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  pushOrder(reqObj) {
    return this.httpClient.post(`${this.APIURL}orders/new`, reqObj).pipe(
      map(resp => resp)
    );
  }
}

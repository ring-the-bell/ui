import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

import { AuthService } from 'src/app/shared/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HeaderService } from 'src/app/common/header/header.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { RegisterService } from 'src/app/register/register.service';
import { CheckoutService } from './checkout.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  checkoutForm: FormGroup;
  modalRef: BsModalRef;
  modalConfig = {
    animated: true,
    keyboard: false,
    backdrop: true,
    ignoreBackdropClick: true,
    class: ''
  };
  returnUrl;
  userInfo;
  cartAmount = 0;
  cartProducts = [];
  loader;
  statesList = [];
  citiesList = [];
  public nameMinLength = 2;
  public nameMaxLength = 15;
  public reqMsg = "This is a required field";

  shipOpts = [{
    type: 'pickup',
    label: 'Pickup at Store'
  }];
  shipAmount = 0;
  shipType;
  storeInfo;

  @ViewChild('loginModal', {static: true}) loginModal: TemplateRef<any>;
  constructor(private authService: AuthService, private modalService: BsModalService, private route: ActivatedRoute, private router: Router, private headerService: HeaderService, private fb: FormBuilder, private registerService: RegisterService, private service: CheckoutService, private toastr: ToastrService) { }

  ngOnInit() {
    this.loader = true
    this.route.queryParamMap.subscribe(qryParams => {
      // console.log(qryParams);
      this.returnUrl = qryParams['params'].returnUrl;
    });
        
    if (this.authService.isLoggedIn()) {
      let role = JSON.parse(this.authService.getUserInfo());
      if (role) {
        if (role.userType != 'shopper') {
          this.router.navigate(['/admin']);
        } else {
          this.userInfo = JSON.parse(this.authService.getUserInfo());
          // this.getAllStates('initial');
          this.formValidations();
          this.setFormVals(this.userInfo);
          this.getCartProducts();
        }
      } else {
        this.router.navigate(['/login'], {queryParams: {returnUrl: this.returnUrl}});
      }
    } else {
      this.router.navigate(['/login'], {queryParams: {returnUrl: this.returnUrl}});
    }
  }

  /**
   * TODO: comment formValidations
   * @description Forms validations
   * @author (Siva Sankar)
   */
  formValidations() {
    this.checkoutForm = this.fb.group({
      fullName: ['', Validators.compose([Validators.required, Validators.minLength(this.nameMinLength), Validators.maxLength(this.nameMaxLength)])],
      mobileNumber: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(15)])],
      address: ['', Validators.compose([Validators.required])],
      email: ['', Validators.compose([Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")])],
      shipping: ['', Validators.required]
    });
  }

  /**
   * TODO: comment setFormVals
   * @description Sets form vals
   * @author (Siva Sankar)
   * @param data 
   */
  setFormVals(data) {
    let obj = {
      fullName: data.fullName,
      mobileNumber: data.mobileNumber,
      address: data.address,
      email: data.email,
      shipping: ''
    }
    this.checkoutForm.setValue(obj);
  }

  /**
   * TODO: comment frmCtrls
   * @description Gets frm ctrls
   */
  get frmCtrls() {
    return this.checkoutForm.controls;
  }

  /**
   * TODO: comment getAllStates
   * @description Gets all states
   * @author (Siva Sankar)
   * @param type 
   */
  getAllStates(type) {
    this.registerService.getAllStates().subscribe(
      resp => { console.log(resp); this.statesList = resp['data']; },
      error => { console.log(error); }
    );

    if (type) {
      this.getAllCities(this.userInfo.stateId);
    }
  }

  /**
   * TODO: comment getAllCities
   * @description Gets all cities
   * @author (Siva Sankar)
   * @param stateId 
   */
  getAllCities(stateId) {
    this.loader = true;
    console.log(stateId);
    if (stateId && stateId > 0) {
      this.registerService.getAllCities(stateId).subscribe(
        resp => { console.log(resp); this.citiesList = resp['data']; this.loader = false; },
        error => { console.log(error); }
      );
    }
  }

  /**
   * TODO: comment getCartProducts
   * @description Gets cart products
   * @author (Siva Sankar)
   */
  getCartProducts() {
    let url = 'cart/items/';
    this.headerService.getCartItems(url).subscribe(
      resp => {
        if (resp && resp['data'] && resp['status_code'] == 200) {
          this.cartAmount = resp['data'].reduce(function(prev, cur) {
            return prev + (parseFloat(cur.productOfferPrice) * parseFloat(cur.productQuantity));
          }, 0);
          this.cartProducts = resp['data'];
          this.getStoreInfo( this.cartProducts[0].storeId );
        } else {
          this.cartProducts = [];
          this.cartAmount = 0;
          this.loader = false;
        }
      },error => {
        this.loader = false;
      }
    );
  }

  /**
   * TODO: comment getStoreInfo
   * @description Gets store info
   * @author (Siva Sankar)
   * @param storeId 
   */
  getStoreInfo(storeId) {
    this.service.storeShipOpts(storeId).subscribe(
      resp => {
        if (resp && resp['data'] && resp['status_code'] == 200) {
          this.storeInfo = resp['data'];
          if (this.storeInfo && Object.keys(this.storeInfo).length > 3) {
            if (this.storeInfo.deliveryAvailable == 0){
              this.shipOpts.push({'label': 'Delivery', 'type': 'delivery'});
            } else {
              this.shipOpts = [{
                type: 'pickup',
                label: 'Pickup at Store'
              }];
            }
          }
        } else {
          this.cartProducts = [];
          this.cartAmount = 0;
        }
        this.loader = false;
      },err => {
        this.loader = false;
        this.toastr.error(err.error.message);
      }
    );
  }

  /**
   * TODO: comment updateShip
   * @description Updates ship
   * @author (Siva Sankar)
   */
  updateShip() {
    console.log(this.shipType);
    if (this.shipType) {
      if (this.shipType.toLowerCase() == 'delivery') {
        if (this.cartAmount < this.storeInfo.minmumPurchase) {
          this.shipAmount = parseFloat(this.storeInfo.deliveryCharges);
        } else {
          this.shipAmount = 0;
        }
      } else {
        this.shipType = 'pickup';
        this.shipAmount = 0;
      }
      this.checkoutForm.patchValue({'shipping': this.shipType});
    }
    
  }

  /**
   * TODO: comment openModal
   * @description Opens modal
   * @author (Siva Sankar)
   * @param template 
   */
  openModal(template: TemplateRef<any>) {
    this.loader = true;
    let reqObj = {
      storeId: this.cartProducts[0].storeId,
      fullName: this.checkoutForm.value.fullName,
      mobileNumber: this.checkoutForm.value.mobileNumber,
      address: this.checkoutForm.value.address,
      shipping: this.checkoutForm.value.shipping
    };
    console.log(reqObj);
    this.service.pushOrder(reqObj).subscribe(
      resp => {
        console.log(resp);
        this.loader = false;
        this.headerService.updateCartItems.next('Update Cart Items');
        this.modalConfig.class = "modal-dialog-centered checkout-auth-modal-cls";    
        this.modalRef = this.modalService.show(template, this.modalConfig);
        setTimeout(() => {
          this.modalCls();
        }, 3500);
      },err => {
        console.log(err);
        this.loader = false;
        this.toastr.error(err.error.message);
      }
    );
  }

  /**
   * TODO: comment modalCls
   * @description Modals cls
   * @author (Siva Sankar)
   */
  modalCls() {
    this.modalRef.hide();
    this.router.navigate(['/groceries/orders']);
  }

}

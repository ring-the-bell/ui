import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user.component';
import { HeaderComponent } from './../../common/header/header.component';
import { FooterComponent } from './../../common/footer/footer.component';
import { UserRoutingModule } from './user-routing.module';
import { ShopComponent } from './shop/shop.component';

import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CollapseModule } from 'ngx-bootstrap/collapse';

import { ViewcartComponent } from './viewcart/viewcart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { ProfileComponent } from './profile/profile.component';
import { OrdersComponent } from './orders/orders.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { ContactUsComponent } from 'src/app/common/contact-us/contact-us.component';

@NgModule({
  declarations: [HomeComponent, UserComponent, HeaderComponent,
    FooterComponent,
    ShopComponent, ViewcartComponent, CheckoutComponent, ProfileComponent, OrdersComponent, ContactUsComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FilterPipeModule,
    UserRoutingModule,
    ModalModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    NgxPaginationModule
  ]
})
export class UserModule { }

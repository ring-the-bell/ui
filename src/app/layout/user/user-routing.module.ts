import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { UserComponent } from "./user.component";
import { HomeComponent } from "./home/home.component";
import { ShopComponent } from "./shop/shop.component";
import { ViewcartComponent } from "./viewcart/viewcart.component";
import { CheckoutComponent } from "./checkout/checkout.component";
import { ProfileComponent } from "./profile/profile.component";
import { RoleGuard } from "src/app/shared/role-guard.service";
import { OrdersComponent } from "./orders/orders.component";
import { ContactUsComponent } from "src/app/common/contact-us/contact-us.component";
const routes: Routes = [
  {
    path: "",
    component: UserComponent,
    // canActivateChild: [RoleGuard],
    children: [
      {
        path: "",
        redirectTo: "home",
        pathMatch: "full"
      },
      {
        path: "home",
        component: HomeComponent,
        data: {
          expectedRole: ["shopper"],
          title: `The First Online Application For Urban/Local Stores To Expand Their Business`
        }
      },
      {
        path: "store/:storeId",
        component: ShopComponent,
        data: {
          expectedRole: ["shopper"],
          title: "Store"
        }
      },
      {
        path: "profile",
        component: ProfileComponent,
        canActivate: [RoleGuard],
        data: {
          expectedRole: ["shopper"],
          title: "Profile"
        }
      },
      {
        path: "orders",
        component: OrdersComponent,
        canActivate: [RoleGuard],
        data: {
          expectedRole: ["shopper"],
          title: "Orders"
        }
      },
      {
        path: "viewcart",
        component: ViewcartComponent,
        canActivate: [RoleGuard],
        data: {
          expectedRole: ["shopper"],
          title: "ViewCart"
        }
      },
      {
        path: "checkout",
        component: CheckoutComponent,
        canActivate: [RoleGuard],
        data: {
          expectedRole: ["shopper"],
          title: "Checkout"
        }
      }
      // {
      //   path: "contact-us",
      //   component: ContactUsComponent,
      //   data: {
      //     expectedRole: ['shopper'],
      //     title: 'Contact Us'
      //   }
      // },
      // {
      //   path: "plans",
      //   loadChildren: () => import('../../common/plans/plans.module').then(m => m.PlansModule)
      // }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}

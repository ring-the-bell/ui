import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Meta } from "@angular/platform-browser";
import { HomeService } from './home.service';
import { Router, NavigationExtras } from '@angular/router';
import { TitleCasePipe } from '@angular/common';

import { HeaderService } from 'src/app/common/header/header.service';
import { AuthService } from 'src/app/shared/auth.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  modalRef: BsModalRef;
  public statesArr;
  public citiesArr;
  public storesArr;
  public storeModalConfig = {
    animated: true,
    keyboard: false,
    backdrop: true,
    ignoreBackdropClick: true,
    class: ''
  };

  public storeObj = {
    stateId: '',
    cityId: '',
    storeName: ''
  };
  public selStateId;
  public noStoreMsg = '0 stores found.';
  public selectedStore;
  loader = true;
  modalCntntObj;
  public locationObj = {
    isEnabled: false,
    lat: null,
    lang: null
  };

  @ViewChild('locationModal', {static: true}) locationModal: TemplateRef<any>;
  
  constructor(private service: HomeService, private router: Router, private titlecasePipe: TitleCasePipe, private headerService: HeaderService, private authService: AuthService, private modalService: BsModalService, private toastr: ToastrService, private cookieService: CookieService, private metaTagService: Meta) { 
    this.metaTagService.addTags([
      {
        name: "keywords",
        content:
          "Online Groceries Store, Groceries Home Delivery, Online Groceries Stores Around You, Online Groceries Stores Near By You, Find Online Groceries Stores Around You, Find Online Groceries Stores Near By You"
      },
      { name: "robots", content: "index, follow" },
      { name: "author", content: "SSR Groups" },
      {
        name: "description",
        content:
          "Shopping for grocery and everyday household products is an integral part of life. With changing time, consumers have started becoming technology savvy, resulting in the widespread use of the Internet and mobile phones. Our portal provides features such as comparative pricing, deals and coupons, home delivery options, and such others."
      }
    ]);
  }

  ngOnInit() {
    this.loader = false;
    // this.getLocation();
    // if (this.authService.isLoggedIn()) {
    //   let role = JSON.parse(this.authService.getUserInfo());
    //   if (role) {
    //     if (role.userType != 'shopper') {
    //       this.router.navigate(['/admin']);
    //     } else {
    //       this.getRecentStores();
    //       this.getAllStates();
    //     }
    //   } else {
    //     this.getRecentStores();
    //     this.getAllStates();
    //   }
    // } else {
        this.getRecentStores();
        this.getAllStates();
    // }
  }


  /**
   * TODO: comment getLocation
   * @description Gets location
   * @author (Siva Sankar)
   */
  getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            // Success function
            this.showPosition.bind(this), 
            // Error function
            this.showError.bind(this), 
            // Options. See MDN for details.
            {
               enableHighAccuracy: true,
               timeout: 5000,
               maximumAge: 0
            });
    } else { 
        console.log("Geolocation is not supported by this browser.");
    }
}

 /**
  * TODO: comment showPosition
  * @description Shows position
  * @author (Siva Sankar)
  * @param position 
  */
 showPosition(position) {
   this.locationObj.isEnabled = true;
   this.locationObj.lat = position.coords.latitude;
   this.locationObj.lang = position.coords.longitude;
   console.log(position.coords.latitude, position.coords.longitude);
   this.getRecentStores();
   this.getAllStates();
}

 /**
  * TODO: comment showError
  * @description Shows error
  * @author (Siva Sankar)
  * @param error 
  */
 showError(error) {
   console.log(this.locationObj);
  switch(error.code) {
    case error.PERMISSION_DENIED:
      this.locationObj.isEnabled = false;
      this.locationObj.lat = this.locationObj.lang = null;
      this.storeModalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
      this.modalRef = this.modalService.show(this.locationModal, this.storeModalConfig);
      console.log("User denied the request for Geolocation.");
      break;
    case error.POSITION_UNAVAILABLE:
      this.locationObj.isEnabled = false;
      this.locationObj.lat = this.locationObj.lang = null;
      this.storeModalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
      this.modalRef = this.modalService.show(this.locationModal, this.storeModalConfig);
      console.log("Location information is unavailable.");
      break;
    case error.TIMEOUT:
      this.locationObj.isEnabled = false;
      this.locationObj.lat = this.locationObj.lang = null;
      this.storeModalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
      this.modalRef = this.modalService.show(this.locationModal, this.storeModalConfig);
      console.log("The request to get user location timed out.");
      break;
    case error.UNKNOWN_ERROR:
      this.locationObj.isEnabled = false;
      this.locationObj.lat = this.locationObj.lang = null;
      this.storeModalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
      this.modalRef = this.modalService.show(this.locationModal, this.storeModalConfig);
      console.log("An unknown error occurred.");
      break;
  }
}

  /**
   * TODO: comment getAllStates
   * @description Gets all states
   * @author (Siva Sankar)
   */
  getAllStates() {
    this.service.getAllStates().subscribe(
      resp => { console.log(resp); this.statesArr = resp['data']; this.loader = false;},
      error => { console.log(error); this.loader = false;}
    );
  }

  /**
   * TODO: comment getAllCities
   * @description Gets all cities
   * @author (Siva Sankar)
   * @param stateId 
   */
  getAllCities(stateId) {
    console.log(stateId);
    if (stateId && stateId > 0) {
      this.loader = true;
      this.service.getAllCities(stateId).subscribe(
        resp => { console.log(resp); this.citiesArr = resp['data']; this.loader = false;},
        error => { console.log(error); this.loader = false;}
      );
    }
  }

  /**
   * TODO: comment getRecentStores
   * @description Gets recent stores
   * @author (Siva Sankar)
   */
  getRecentStores() {
      this.service.getRecentStores(this.locationObj).subscribe(
        resp => { 
          console.log(resp); 
          // if (resp['data'] && resp['data'].length > 0) {
          //   resp['data'].forEach(obj => {
          //     if (obj && obj.imageUrl) {
          //       // console.log(obj);
          //       if (obj.imageUrl.indexOf('/home/c8ugqa8m8lxk/public_html') != -1) {
          //         let spltStr = obj.imageUrl.split('/home/c8ugqa8m8lxk/public_html')[1];
          //         obj.imageUrl = 'http://muscletechgym.com' + spltStr;
          //       }
          //     }
          //   });
          // }
          this.storesArr = resp['data']; 
          this.loader = false;
        },
        error => { console.log(error); }
      );
  }

  /**
   * TODO: comment getStoreInfo
   * @description Gets store info
   * @author (Siva Sankar)
   */
  getStoreInfo() {
    // console.log(val);
    if (this.storeObj.stateId || this.storeObj.cityId || this.storeObj.storeName) {
      this.loader = true;
      this.service.getStoreInfo(this.storeObj).subscribe(
        resp => { console.log(resp); this.storesArr = resp['data']; this.loader = false;},
        error => { console.log(error); }
      );
    }
  }

  /**
   * TODO: comment goToStore
   * @description Go to store
   * @author (Siva Sankar)
   * @param store 
   * @param template 
   */
  goToStore(store, template: TemplateRef<any>) {
    this.selectedStore = null;
    let currStore = null;
    if (this.cookieService.get('currentStore'))
      currStore = JSON.parse(this.cookieService.get('currentStore'));
    this.selectedStore = store;
    if (store && currStore) {
      if (currStore.storeId == store.storeId) {
        let navigationExtras: NavigationExtras = {
          queryParams: store
        };
        this.router.navigate([`/groceries/store`, btoa(store.storeId)]);
      } else {
        this.modalCntntObj = {
          imgUrl: 'assets/images/modal-icons/warning.png',
          title: 'Multilple Stores Selection is not available.!',
          subTitle: 'If yes, cart items will be removed(if selected)',
        }
        this.storeModalConfig.class = "modal-dialog-centered checkout-auth-modal-cls";
        this.modalRef = this.modalService.show(template, this.storeModalConfig);
      }
    } else {
      let navigationExtras: NavigationExtras = {
        queryParams: store
      };
      this.router.navigate([`/groceries/store`, btoa(store.storeId)]);
    }
  }

  /**
   * TODO: comment emptyCart
   * @description Emptys cart
   * @author (Siva Sankar)
   */
  emptyCart() {
    this.modalRef.hide();
    this.loader = true;
    let currStore = JSON.parse(this.cookieService.get('currentStore'));
    let store = this.selectedStore;
    let guestToken = this.cookieService.get('guestToken');
    this.service.emptyCart(currStore.storeId).subscribe(
      resp => {
        this.loader = false;
        if (resp && resp['status_code'] == 200) {
          this.headerService.updateCartItems.next('Update Cart Items');
          let navigationExtras: NavigationExtras = {
            queryParams: store
          };
          this.router.navigate([`/groceries/store`, btoa(store.storeId)]);
        }
      }, err => {
        this.loader = false;
        this.toastr.error(err.error.message);
      }
    );
  }

}

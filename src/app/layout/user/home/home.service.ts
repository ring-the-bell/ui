import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment getAllStates
   * @description Gets all states
   * @author (Siva Sankar)
   * @returns  
   */
  getAllStates() {
    return this.httpClient.get(`${this.APIURL}states`).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment getAllCities
   * @description Gets all cities
   * @author (Siva Sankar)
   * @param stateId 
   * @returns  
   */
  getAllCities(stateId) {
    return this.httpClient.get(`${this.APIURL}cities/${stateId}`).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment getRecentStores
   * @description Gets recent stores
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  getRecentStores(reqObj) {
    return this.httpClient.post(`${this.APIURL}stores`, reqObj).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment getStoreInfo
   * @description Gets store info
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  getStoreInfo(reqObj) {
    return this.httpClient.post(`${this.APIURL}store/info`, reqObj).pipe(
      map(resp => resp)
    );
  }   

  /**
   * TODO: comment emptyCart
   * @description Emptys cart
   * @author (Siva Sankar)
   * @param storeId 
   * @returns  
   */
  emptyCart(storeId) {
    return this.httpClient.put(`${this.APIURL}cart/remove/${storeId}`, {}).pipe(
      map(resp => resp)
    );
  }
}

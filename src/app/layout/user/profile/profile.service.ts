import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

/**
 * TODO: comment getProfileDetails
 * @description Gets profile details
 * @author (Siva Sankar)
 * @returns  
 */
getProfileDetails() {
    return this.httpClient.get(`${this.APIURL}user/profile`).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment updatePassword
   * @description Updates password
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  updatePassword(reqObj) {
    return this.httpClient.post(`${this.APIURL}password/update`, reqObj).pipe(
      map(resp => resp)
    );
  }
}

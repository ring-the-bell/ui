import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from './profile.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  pwdFrm: FormGroup;
  profileAccordioin = false;
  pwdAccordioin = true;
  loader;
  pwdErr;
  profileInfo;

  pwdMinLength = 6;
  pwdMaxLength = 15;

  constructor(private fb: FormBuilder, private service: ProfileService, private toastr: ToastrService) { }

  ngOnInit() {
    this.formValidations();
    this.getProfileDetails();
  }

  /**
   * TODO: comment formValidations
   * @description Forms validations
   * @author (Siva Sankar)
   */
  formValidations() {
    this.pwdFrm = this.fb.group({
      oldPassword: ['', Validators.compose([Validators.required, Validators.minLength(this.pwdMinLength), Validators.maxLength(this.pwdMaxLength)])],
      newPassword: ['', Validators.compose([Validators.required, Validators.minLength(this.pwdMinLength), Validators.maxLength(this.pwdMaxLength)])],
      confirmPassword: ['', Validators.compose([Validators.required, Validators.minLength(this.pwdMinLength), Validators.maxLength(this.pwdMaxLength)])]      
    });
  }

  /**
   * TODO: comment frmCtrls
   * @description Gets frm ctrls
   */
  get frmCtrls() {
    return this.pwdFrm.controls;
  }
  
/**
 * TODO: comment openAccord
 * @description Opens accord
 * @author (Siva Sankar)
 * @param type 
 */
openAccord(type) {
    if (type == 'profile') {
      this.profileAccordioin = false;
      this.pwdAccordioin = !this.profileAccordioin;
    } else {
      this.pwdAccordioin = false;
      this.profileAccordioin = !this.pwdAccordioin;
    }
  }

/**
 * TODO: comment getProfileDetails
 * @description Gets profile details
 * @author (Siva Sankar)
 */
getProfileDetails() {
    this.loader = true;
    this.service.getProfileDetails().subscribe(
      resp => {
        console.log(resp);
        if (resp && resp['data']) {
          this.profileInfo = resp['data'];
        }
        this.loader = false;
      },err => {
        this.loader = false;
      }
    );
  }

  /**
   * TODO: comment onSubmit
   * @description Determines whether submit on
   * @author (Siva Sankar)
   */
  onSubmit() {
    if (this.pwdFrm.valid) {
      this.loader = true;
      let formObj = this.pwdFrm.value;
      // console.log(formObj, formObj.password, formObj.confirmPassword)
      if (formObj.newPassword != formObj.confirmPassword) {
        this.loader = false;
        this.pwdErr = 'Passwords should match';
      } else {
        this.pwdErr = null;
        delete formObj['confirmPassword'];
        this.service.updatePassword(formObj).subscribe(
          resp => {
            this.pwdFrm.reset();
            this.profileAccordioin = true;
            this.pwdAccordioin = true;
            this.loader = false;
            if (resp) {
              this.toastr.success(resp['message']);
            } else {
              this.toastr.error(resp['Something went wrong. pLease try agian later']);
            };
          },
          err => {
            this.pwdFrm.reset();
            this.loader = false;
            if (err) {
              this.toastr.error(err.error.message);
            } else {
              this.toastr.error(err.error.message);
            }
          }
        );
      }
    }
  }

}

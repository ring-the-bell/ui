import { Component, OnInit } from '@angular/core';

declare var $;
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $(window).scroll(function () {
      var scroll = $(window).scrollTop();
      if (scroll > 200) {
        $('#page-navigation').addClass('bg-white').removeClass('bg-transparent');
        $('#page-navigation').addClass('navbar-light').removeClass('navbar-dark');
      } else {
        $('#page-navigation').addClass('bg-transparent').removeClass('bg-white');
        $('#page-navigation').addClass('navbar-dark').removeClass('navbar-light');
      }
    });
  }

}

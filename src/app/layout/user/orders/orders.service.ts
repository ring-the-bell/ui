import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {

  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment getInvoices
   * @description Gets invoices
   * @author (Siva Sankar)
   * @returns  
   */
  getInvoices() {
    return this.httpClient.get(`${this.APIURL}user/invoice`).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment getOrdersDetails
   * @description Gets orders details
   * @author (Siva Sankar)
   * @param invoiceId 
   * @returns  
   */
  getOrdersDetails(invoiceId) {
    return this.httpClient.get(`${this.APIURL}user/invoice/order/${invoiceId}`).pipe(
      map(resp => resp)
    );
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ShopService {
  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment getCategories
   * @description Gets categories
   * @author (Siva Sankar)
   * @returns  
   */
  getCategories() {
    return this.httpClient.get(`${this.APIURL}categories/`).pipe(
      map(resp => resp)
    );
  }

/**
 * TODO: comment getStoreInfo
 * @description Gets store info
 * @author (Siva Sankar)
 * @param storeId 
 * @returns  
 */
getStoreInfo(storeId) {
    return this.httpClient.get(`${this.APIURL}store/details/${storeId}`).pipe(
      map(resp => resp)
    );
  }
  
  /**
   * TODO: comment getProducts
   * @description Gets products
   * @author (Siva Sankar)
   * @param storeId 
   * @returns  
   */
  getProducts(storeId) {
    return this.httpClient.get(`${this.APIURL}store/products/${storeId}`).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment searchProduct
   * @description Searchs product
   * @author (Siva Sankar)
   * @param storeId 
   * @param name 
   * @returns  
   */
  searchProduct(storeId,name) {
    return this.httpClient.get(`${this.APIURL}store/products/choice/${storeId}/${name}`).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment updateUserCart
   * @description Updates user cart
   * @author (Siva Sankar)
   * @param prod 
   * @returns  
   */
  updateUserCart(prod) {
    return this.httpClient.post(`${this.APIURL}cart/items/`, prod).pipe(
      map(resp => resp)
    );
  }
}

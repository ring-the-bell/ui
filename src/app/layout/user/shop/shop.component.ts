import { Component, OnInit } from '@angular/core';

import { ShopService } from './shop.service';
import { HeaderService } from 'src/app/common/header/header.service';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from 'src/app/shared/auth.service';
import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss']
})
export class ShopComponent implements OnInit {

  loader = false;
  itemsPerSlide = 10;
  singleSlideOffset = true;
  noWrap = true;
  categoriesArr;
  storeProds;
  storeInfo;
  noProductsMsg = 'awww... no products found.';
  productName = null;
  slides = [
    { name: 'Grocery & Staples', image: './../../../../assets/images/grocery-staples.png' },
    { name: 'Vegetables & Fruits', image: './../../../../assets/images/vegetables-fruits.jpg' },
    { name: 'Personal Care', image: './../../../../assets/images/personal-care.jpg' },
    { name: 'Household Items', image: './../../../../assets/images/household-items.jpg' },
    { name: 'Home & Kitchen', image: './../../../../assets/images/home-kitchen.jpg' },
    { name: 'Snacks', image: './../../../../assets/images/snacks.jpg' },
    { name: 'Beverages', image: './../../../../assets/images/beverage.png' },
    { name: 'Breakfast & Dairy', image: './../../../../assets/images/breakfast-dairy.jpg' },
    { name: 'Instant Food', image: './../../../../assets/images/instant-food.jpg' },
    { name: 'Baby Care', image: './../../../../assets/images/baby-care.jpg' }
  ];

  // this.myData.next(data);

  constructor(private service: ShopService, private headerService: HeaderService, private route: ActivatedRoute, private authService: AuthService, private router: Router, private toastr: ToastrService, private cookieService: CookieService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      console.log(params);
      if (params && params.storeId && parseInt(atob(params.storeId)) > 0) {
        this.loader = true;
        this.getStoreInfo(parseInt(atob(params.storeId)));
        // if (this.authService.isLoggedIn()) {
        //   let role = JSON.parse(this.authService.getUserInfo());
        //   if (role) {
        //     if (role.userType != 'shopper') {
        //       this.router.navigate(['/admin']);
        //     } else {
        //       this.getStoreInfo(parseInt(atob(params.storeId)));
        //     }
        //   } else {
        //     this.authService.logout();
        //     this.router.navigate(['/login']);
        //   }
        // } else {
        //   this.authService.logout();
        //   this.router.navigate(['/login']);
        // }
      } else {
        // this.authService.logout();
        // this.router.navigate(['/login']);
        this.router.navigate(['/groceries/home']);
      }      
    });
  }

  /**
   * TODO: comment getStoreInfo
   * @description Gets store info
   * @author (Siva Sankar)
   * @param storeId 
   */
  getStoreInfo(storeId) {
    this.service.getStoreInfo(storeId).subscribe(
      resp => {
        this.loader = false;
        this.storeInfo = resp['data'];
        this.cookieService.set('currentStore', JSON.stringify(resp['data']));
        this.getProducts();
      },err => {
        this.loader = false;
        this.authService.logout();
        this.router.navigate(['/login']);
      }
    );
  }

  /**
   * TODO: comment getCategories
   * @description Gets categories
   * @author (Siva Sankar)
   */
  getCategories() {
    this.service.getCategories().subscribe(
      resp => { console.log(resp); this.categoriesArr = resp['data'] },
      error => { console.log(error); }
    );
  }

  /**
   * TODO: comment getProducts
   * @description Gets products
   * @author (Siva Sankar)
   */
  getProducts() {
    if (Object.keys(this.storeInfo).length > 1 && this.storeInfo.storeId > 0) {
      this.service.getProducts(this.storeInfo.storeId).subscribe(
        resp => {
          if (resp && resp['data']) {
            if (resp['data'].length > 0) {
              resp['data'].filter(prod => {
                prod.prodQty = 0;
              });
              this.storeProds = resp['data'];
            } else {
              this.storeProds = [];
            }
          } else {
            this.storeProds = [];
          }
          this.loader = false;
        },
        error => {
          console.log(error);
          this.loader = false;
        }
      );
    } else {

    }
  }

  /**
   * TODO: comment findProduct
   * @description Finds product
   * @author (Siva Sankar)
   */
  findProduct() {
    if (Object.keys(this.storeInfo).length > 1 && this.storeInfo.storeId > 0 && this.productName) {
      this.loader = true;
        this.service.searchProduct(this.storeInfo.storeId, this.productName).subscribe(
          resp => {
            if (resp && resp['data']) {
              if (resp['data'].length > 0) {
                resp['data'].filter(prod => {
                  prod.prodQty = 0;
                });
                this.storeProds = resp['data'];
              } else {
                this.storeProds = [];
              }
            } else {
              this.storeProds = [];
            }
            this.loader = false;
          },err => {
            this.loader = false;
            console.log(err);
          }
        );
    }
}

/**
 * TODO: comment updateQty
 * @description Updates qty
 * @author (Siva Sankar)
 * @param prod 
 * @param action 
 */
updateQty(prod, action) {
    if (this.authService.isLoggedIn()) {
      this.loader = true;
      console.log(prod, action);
      if (prod && action) {
        let qtyVal;
        if (action == 'minus') {
          if (prod.prodQty >= 1) {
            prod.prodQty = prod.prodQty - 1;
          } else {
            prod.prodQty = 0;
          }
        } else if (action == 'plus') {
          prod.prodQty = prod.prodQty + 1;
        } else if (action == 'focus') {
          if (prod.prodQty < 0) {
            prod.prodQty = 0;
          }
        }
      }
      this.loader = false;
    } else {
      this.router.navigate(['/login'], { queryParams: { returnUrl: this.router.url }});
    }
  }

  /**
   * TODO: comment addToCart
   * @description Adds to cart
   * @author (Siva Sankar)
   * @param item 
   */
  addToCart(item) {
    this.loader = true;
    console.log(item);

    let cartObj;
    if (this.authService.isLoggedIn()) {
      cartObj = {
        prodQty: item.prodQty,
        productId: item.productId,
        storeId: this.storeInfo.storeId
      };
      this.updateUserCart(cartObj);
    } else {
      this.authService.logout();
    }
  }

  /**
   * TODO: comment updateUserCart
   * @description Updates user cart
   * @author (Siva Sankar)
   * @param cartObj 
   */
  updateUserCart(cartObj) {
    this.service.updateUserCart(cartObj).subscribe(
      resp => {
        this.loader = false;
        this.toastr.success(resp['message']);
        this.headerService.updateCartItems.next('Update Cart Items');

      },err => {
        this.loader = false;
        this.toastr.error(err.error.message);
        this.headerService.updateCartItems.next('Update Cart Items');
      }
    );
  }
  
}

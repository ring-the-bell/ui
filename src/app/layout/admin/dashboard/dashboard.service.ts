import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment getStats
   * @description Gets stats
   * @author (Siva Sankar)
   * @param type 
   * @returns  
   */
  getStats(type: string) {
    return this.httpClient.get(`${this.APIURL}statistics/${type}`).pipe(
      map(resp => resp)
    );
  }
}

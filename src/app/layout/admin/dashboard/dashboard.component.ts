import { Component, OnInit } from '@angular/core';

import { DashboardService } from './dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public productsCnt;
  public ordersCnt;
  public loader;

  constructor(private service: DashboardService) { }

  ngOnInit() {
    this.loader = true;
    this.getStats('products');
    this.getStats('orders');
  }

  /**
   * TODO: comment getStats
   * @description Gets stats
   * @author (Siva Sankar)
   * @param type 
   */
  getStats(type) {
    this.service.getStats(type).subscribe(
      resp => {
        console.log(resp);
        if (type == 'products') {
          this.productsCnt = resp['data']['count(*)'];
        } else {
          this.ordersCnt = resp['data']['count(*)'];
        }
        this.loader = false;
      },
      error => {
        console.log(error);
      }
    );
  }
}

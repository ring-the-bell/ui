import { Component, OnInit } from "@angular/core";
import { StoreDetailsService } from "./store-details.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { RegisterService } from "src/app/register/register.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-store-details",
  templateUrl: "./store-details.component.html",
  styleUrls: ["./store-details.component.scss"]
})
export class StoreDetailsComponent implements OnInit {
  storeForm: FormGroup;
  accForm: FormGroup;
  loader;
  storeAccordioin;
  pwdAccordioin;
  nameMinLength = 2;
  nameMaxLength = 15;
  pwdMinLength = 6;
  pwdMaxLength = 15;

  public reqMsg = "This is a required field";
  public pwdErr = null;
  public fileErr = null;
  public uploaded = false;
  public imgBase64 = "assets/images/placeholder.jpg";
  public storeLogo = null;

  public statesList;
  public citiesList;
  public isDeliveryInfoActive = false;
  public orgResp;

  constructor(
    private service: StoreDetailsService,
    private fb: FormBuilder,
    private registerService: RegisterService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.storeAccordioin = false;
    this.pwdAccordioin = true;
    this.storeFormValidations();
    this.accFormValidations();
    this.getStoreDetails();
  }

  /**
   * TODO: comment storeFormValidations
   * @description Stores form validations
   * @author (Siva Sankar)
   */
  storeFormValidations() {
    this.storeForm = this.fb.group({
      storeName: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(this.nameMinLength),
          Validators.maxLength(this.nameMaxLength)
        ])
      ],
      email: [
        "",
        Validators.compose([
          Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$")
        ])
      ],
      address: ["", Validators.compose([Validators.required])],
      mobileNumber: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(15),
          Validators.pattern("^[0-9 ]*$")
        ])
      ],
      delivery: [""],
      amount: [""],
      charges: [""]
    });
  }

  /**
   * TODO: comment accFormValidations
   * @description Accs form validations
   * @author (Siva Sankar)
   */
  accFormValidations() {
    this.accForm = this.fb.group({
      oldPassword: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(this.pwdMinLength),
          Validators.maxLength(this.pwdMaxLength)
        ])
      ],
      newPassword: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(this.pwdMinLength),
          Validators.maxLength(this.pwdMaxLength)
        ])
      ],
      confirmPassword: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(this.pwdMinLength),
          Validators.maxLength(this.pwdMaxLength)
        ])
      ]
    });
  }

  /**
   * TODO: comment openAccord
   * @description Opens accord
   * @author (Siva Sankar)
   * @param type
   */
  openAccord(type) {
    if (type == "store") {
      this.storeAccordioin = false;
      this.pwdAccordioin = !this.storeAccordioin;
    } else {
      this.pwdAccordioin = false;
      this.storeAccordioin = !this.pwdAccordioin;
    }
  }

  /**
   * TODO: comment storeFrmCtrls
   * @description Gets store frm ctrls
   */
  get storeFrmCtrls() {
    return this.storeForm.controls;
  }

  /**
   * TODO: comment accFrmCtrls
   * @description Gets acc frm ctrls
   */
  get accFrmCtrls() {
    return this.accForm.controls;
  }

  /**
   * TODO: comment checkRecord
   * @description Checks record
   * @author (Siva Sankar)
   * @param val
   * @param type
   */
  checkRecord(val, type) {
    console.log(val, type);
    if (val && type) {
      let fieldVal;
      if (val == "storeName") {
        if (
          this.storeForm.value.storeName &&
          this.orgResp.storeName != this.storeForm.value.storeName
        ) {
          this.loader = true;
          fieldVal = this.storeForm.value.storeName;
          this.verifyRecors(val, fieldVal);
        }
      } else if (
        val == "number" &&
        this.orgResp.storeNumber != this.storeForm.value.mobileNumber
      ) {
        if (this.storeForm.value.mobileNumber) {
          this.loader = true;
          fieldVal = this.storeForm.value.mobileNumber;
          this.verifyRecors(val, fieldVal);
        }
      } else if (
        val == "email" &&
        this.orgResp.email != this.storeForm.value.email
      ) {
        if (this.storeForm.value.email) {
          this.loader = true;
          fieldVal = this.storeForm.value.email;
          this.verifyRecors(val, fieldVal);
        }
      }
    }
  }

  /**
   * TODO: comment verifyRecors
   * @description Verifys recors
   * @author (Siva Sankar)
   * @param val
   * @param fieldVal
   */
  verifyRecors(val, fieldVal) {
    if (val && fieldVal) {
      this.registerService.verifyRecors(val, fieldVal).subscribe(
        resp => {
          this.loader = false;
        },
        err => {
          console.log(err);
          if (err) {
            this.loader = false;
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error(err.error.message);
          }
        }
      );
    } else {
      this.loader = false;
    }
  }

  /**
   * TODO: comment getDeliveryInfo
   * @description Gets delivery info
   * @author (Siva Sankar)
   */
  getDeliveryInfo() {
    console.log(this.storeForm.value);
    if (this.storeForm.value.delivery) {
      this.isDeliveryInfoActive = true;
      this.storeForm
        .get("amount")
        .setValidators([
          Validators.compose([Validators.required, Validators.min(0)])
        ]);
      this.storeForm
        .get("charges")
        .setValidators([
          Validators.compose([Validators.required, Validators.min(0)])
        ]);
    } else {
      this.isDeliveryInfoActive = false;
      this.storeForm.get("amount").clearValidators();
      this.storeForm.get("charges").clearValidators();
      this.storeForm.get("amount").setValue("");
      this.storeForm.get("charges").setValue("");
    }
  }

  /**
   * TODO: comment fileUpload
   * @description Files upload
   * @author (Siva Sankar)
   * @param event
   */
  fileUpload(event) {
    this.loader = true;
    this.uploaded = true;
    let reader = new FileReader();
    console.log(event.target.files[0]);
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      let ext = file.name.substring(file.name.lastIndexOf(".") + 1);
      if (
        ext.toLowerCase() == "png" ||
        ext.toLowerCase() == "jpeg" ||
        ext.toLowerCase() == "jpg"
      ) {
        this.storeLogo = file;
        let img = new Image();
        img.src = window.URL.createObjectURL(file);
        reader.readAsDataURL(file);
        reader.onload = () => {
          setTimeout(() => {
            const width = img.naturalWidth;
            const height = img.naturalHeight;

            window.URL.revokeObjectURL(img.src);
            this.imgBase64 = reader.result as string;
            this.fileErr = null;
            // console.log(width + '*' + height, reader.result);
            if (width < 320 && height < 320) {
              // this.fileErr = "Size should be greater than 320*320";
            } else {
              // console.log(reader.result);
              this.imgBase64 = reader.result as string;
              this.fileErr = null;
              // document.querySelector('#product-img').setAttribute('src', reader.result as string);
              // console.log(document.querySelector('#product-img'));
            }
            this.loader = false;
          }, 2000);
        };
      } else {
        this.uploaded = false;
        this.fileErr = null;
        this.storeLogo = null;
        this.loader = false;
        this.toastr.error("Only jpeg/jpg, and png files are allowed.");
      }
    } else {
      this.imgBase64 = "assets/images/placeholder.jpg";
      this.uploaded = false;
      this.fileErr = null;
      this.storeLogo = null;
      this.loader = false;
    }
  }

  /**
   * TODO: comment getStoreDetails
   * @description Gets store details
   * @author (Siva Sankar)
   */
  getStoreDetails() {
    this.loader = true;
    this.service.getStoreDetails().subscribe(resp => {
      console.log(resp);
      if (resp && resp["data"]) {
        this.orgResp = resp["data"];
        let data = resp["data"];
        let obj = {
          storeName: data.storeName ? data.storeName : "",
          email: data.email ? data.email : "",
          address: data.address ? data.address : "",
          mobileNumber: data.storeNumber ? data.storeNumber : "",
          delivery:
            data.deliveryAvailable == "1" || data.deliveryAvailable == 1
              ? false
              : true,
          amount: data.minmumPurchase ? data.minmumPurchase : "",
          charges: data.deliveryCharges ? data.deliveryCharges : ""
        };

        if (data.imageUrl) {
          this.imgBase64 = data.imageUrl;
          this.uploaded = true;
          this.fileErr = null;
        }
        this.storeForm.setValue(obj);
        if (data.deliveryAvailable) {
          this.getDeliveryInfo();
        }
      }
      this.loader = false;
    });
  }

  /**
   * TODO: comment onStoreSubmit
   * @description Determines whether store submit on
   * @author (Siva Sankar)
   */
  onStoreSubmit() {
    this.loader = true;
    console.log(this.storeForm.valid, this.storeForm.value);
    if (this.storeForm.valid) {
      let formObj = this.storeForm.value;
      // console.log(formObj, formObj.password, formObj.confirmPassword)
      if (this.uploaded && this.fileErr == null) {
        let fd = new FormData();
        Object.entries(formObj).forEach(([key, value]) => {
          // console.log(`${key}: ${value}`);
          fd.append(key, formObj[key]);
        });
        fd.append("storeLogo", this.storeLogo);
        this.service.updateStore(fd).subscribe(
          resp => {
            if (resp) {
              console.log(resp);
              if (resp["status_code"] == 200) {
                this.toastr.success(resp["message"]);
                this.storeAccordioin = true;
                this.pwdAccordioin = true;
                this.loader = false;
              } else {
                this.toastr.error(resp["message"]);
              }
            } else {
              this.toastr.error("Something went wrong. pLease try agian later");
            }
          },
          error => {
            this.loader = false;
            if (error) {
              this.toastr.error(error.error.message);
            } else {
              this.toastr.error("Something went wrong. pLease try agian later");
            }
          }
        );
      } else {
        this.pwdErr = null;
        this.fileErr = "Logo is required";
        this.loader = false;
      }
    }
  }

  /**
   * TODO: comment onPwdSubmit
   * @description Determines whether pwd submit on
   * @author (Siva Sankar)
   */
  onPwdSubmit() {
    if (this.accForm.valid) {
      this.loader = true;
      let formObj = this.accForm.value;
      // console.log(formObj, formObj.password, formObj.confirmPassword)
      if (formObj.newPassword != formObj.confirmPassword) {
        this.loader = false;
        this.pwdErr = "Passwords should match";
      } else {
        this.pwdErr = null;
        delete formObj["confirmPassword"];
        this.service.updatePassword(formObj).subscribe(
          resp => {
            this.accForm.reset();
            this.storeAccordioin = true;
            this.pwdAccordioin = true;
            this.loader = false;
            if (resp) {
              this.toastr.success(resp["message"]);
            } else {
              this.toastr.error(
                resp["Something went wrong. pLease try agian later"]
              );
            }
          },
          err => {
            this.accForm.reset();
            this.loader = false;
            if (err) {
              this.toastr.error(err.error.message);
            } else {
              this.toastr.error(err.error.message);
            }
          }
        );
      }
    }
  }
}

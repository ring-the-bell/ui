import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class StoreDetailsService {
  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment getStoreDetails
   * @description Gets store details
   * @author (Siva Sankar)
   * @returns  
   */
  getStoreDetails() {
    return this.httpClient.get(`${this.APIURL}store/details`).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment updatePassword
   * @description Updates password
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  updatePassword(reqObj) {
    return this.httpClient.post(`${this.APIURL}password/update`, reqObj).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment updateStore
   * @description Updates store
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  updateStore(reqObj) {
    return this.httpClient.post(`${this.APIURL}store/update`, reqObj).pipe(
      map(resp => resp)
    );
  }
}

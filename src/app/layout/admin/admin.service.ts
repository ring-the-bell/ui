import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AdminService {
  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment getStoreInfo
   * @description Gets store info
   * @author (Siva Sankar)
   * @returns  
   */
  getStoreInfo() {
    return this.httpClient.get(`${this.APIURL}store/profile`).pipe(
      map(resp => resp)
    );
  }
}

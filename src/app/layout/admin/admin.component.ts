import { Component, OnInit } from '@angular/core';
import { TitleCasePipe } from '@angular/common';

import { AuthService } from 'src/app/shared/auth.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AdminService } from './admin.service';

declare var jQuery;
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  routerMenu;
  rootModule = '/admin/';
  loader = false;
  storeInfo = {
    imageUrl: '',
    storeName: '',
    registeredDate: '',
    subscriptionDate: ''
  };

  subscriptionAlert;

  constructor(private router: Router, private authService: AuthService, private titlecasePipe: TitleCasePipe, private toastr: ToastrService, private adminService: AdminService) { }

  ngOnInit() {
    this.toggleAction();
    this.getStoreInfo();
    this.routerMenu = [
      { name: 'profile', url: this.rootModule + '/profile', icon: 'fa-address-book' },
      { name: 'dashboard', url: this.rootModule + '/dashboard', icon: 'fa-tachometer-alt' },
      { name: 'products', url: this.rootModule + '/products', icon: 'fa-cubes' },
      { name: 'orders', url: this.rootModule + '/orders', icon: 'fa-truck' },
      // { name: 'notifications', url: this.rootModule + '/notifications', icon: 'fa bell' },
    ]
  }

  /**
   * TODO: comment toggleAction
   * @description Toggles action
   * @author (Siva Sankar)
   */
  toggleAction() {
    jQuery(function ($) {

      $(".sidebar-dropdown > a").click(function () {
        $(".sidebar-submenu").slideUp(200);
        if (
          $(this)
            .parent()
            .hasClass("active")
        ) {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .parent()
            .removeClass("active");
        } else {
          $(".sidebar-dropdown").removeClass("active");
          $(this)
            .next(".sidebar-submenu")
            .slideDown(200);
          $(this)
            .parent()
            .addClass("active");
        }
      });

      $("#close-sidebar").click(function () {
        $(".page-wrapper").removeClass("toggled");
      });
      $("#show-sidebar").click(function () {
        $(".page-wrapper").addClass("toggled");
      });
    });
  }

  /**
   * TODO: comment getStoreInfo
   * @description Gets store info
   * @author (Siva Sankar)
   */
  getStoreInfo() {
    this.adminService.getStoreInfo().subscribe(
      resp => {
        console.log(resp);
        this.subscriptionAlert = this.calculateDiff(resp['data'].registeredDate);
        this.storeInfo = resp['data'];
      }
    );
  }

/**
 * TODO: comment calculateDiff
 * @description Calculates diff
 * @author (Siva Sankar)
 * @param dateSent 
 * @returns  
 */
calculateDiff(dateSent){
    let currentDate = new Date();
    let cretedDate = new Date(dateSent);
 
     return Math.floor((Date.UTC(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()) - Date.UTC(cretedDate.getFullYear(), cretedDate.getMonth(), cretedDate.getDate()) ) /(1000 * 60 * 60 * 24));
   }

  /**
   * TODO: comment logout
   * @description Logouts admin component
   * @author (Siva Sankar)
   */
  logout() {
    this.loader = true;
    setTimeout (() => {
      this.authService.logout();
      this.toastr.success("Logout successfully.");
      this.loader = false;
      this.router.navigate(['/login']);
    }, 1000);
  }

}

import { Component, OnInit, TemplateRef } from "@angular/core";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { OrdersService } from "./orders.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-orders",
  templateUrl: "./orders.component.html",
  styleUrls: ["./orders.component.scss"]
})
export class OrdersComponent implements OnInit {
  modalRef: BsModalRef;
  invoivesList = [];
  loader;
  pagination = {
    itemsPerPage: 5,
    currentPage: 1,
    totalItems: 0
  };
  orderModalConfig = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: true,
    class: ""
  };
  selectedInvoice;
  orderDetails;
  orderAmount = 0;
  statusOfInvoice = null;
  orderStatusArr = [
    "waiting for approval",
    "canceled",
    "approved",
    "on the way",
    "delivered"
  ];
  constructor(
    private service: OrdersService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.getInvoices();
  }

  /**
   * TODO: comment getInvoices
   * @description Gets invoices
   * @author (Siva Sankar)
   */
  getInvoices() {
    this.loader = true;
    this.service.getInvoices().subscribe(
      resp => {
        if (resp && resp["data"]) {
          this.pagination.totalItems = resp["data"].length;
          this.invoivesList = resp["data"];
        }
        this.loader = false;
      },
      error => {
        this.loader = false;
      }
    );
  }

  /**
   * TODO: comment pageChanged
   * @description Pages changed
   * @author (Siva Sankar)
   * @param e
   */
  pageChanged(e) {
    // console.log(e);
    this.pagination.currentPage = e;
  }

  /**
   * TODO: comment getOrderDetails
   * @description Gets order details
   * @author (Siva Sankar)
   * @param template
   * @param invoiceData
   */
  getOrderDetails(template: TemplateRef<any>, invoiceData) {
    if (invoiceData) {
      this.loader = true;
      this.service.getOrdersDetails(invoiceData.invoiceId).subscribe(
        resp => {
          console.log(resp);
          if (resp && resp["data"] && resp["data"].length > 0) {
            invoiceData.shippingCharges = parseFloat(
              invoiceData.shippingCharges
            );
            this.selectedInvoice = invoiceData;
            this.orderAmount = resp["data"].reduce(function(prev, cur) {
              return (
                prev +
                parseFloat(cur.productPrice) * parseFloat(cur.productQuantity)
              );
            }, 0);
            this.orderDetails = resp["data"];
            this.orderModalConfig.class =
              "modal-lg modal-dialog-centered checkout-auth-modal-cls";
            this.modalRef = this.modalService.show(
              template,
              this.orderModalConfig
            );
          } else {
            this.orderDetails = [];
            this.selectedInvoice = null;
            this.orderAmount = 0;
          }
          this.loader = false;
        },
        error => {
          this.loader = false;
        }
      );
    }
  }

  /**
   * TODO: comment openModal
   * @description Opens modal
   * @author (Siva Sankar)
   * @param invoice
   * @param template
   */
  openModal(invoice, template: TemplateRef<any>) {
    if (invoice) {
      this.statusOfInvoice = null;
      this.statusOfInvoice = { ...invoice, ...this.statusOfInvoice };
      this.orderModalConfig.class =
        "modal-dialog-centered checkout-auth-modal-cls";
      this.modalRef = this.modalService.show(template, this.orderModalConfig);
    }
  }

  /**
   * TODO: comment changeStatus
   * @description Changes status
   * @author (Siva Sankar)
   */
  changeStatus() {
    this.loader = true;
    if (this.statusOfInvoice) {
      this.modalRef.hide();
      let reqObj = {
        invoiceId: this.statusOfInvoice.invoiceId,
        invoiceStatus: this.statusOfInvoice.orderStatus
      };
      this.service.changeInvoiceStatus(reqObj).subscribe(
        resp => {
          this.statusOfInvoice = null;
          this.loader = false;
          this.getInvoices();
          this.toastr.success(resp["message"]);
        },
        err => {
          this.statusOfInvoice = null;
          this.loader = false;
          this.getInvoices();
          this.toastr.error(err.error.message);
        }
      );
      console.log(this.statusOfInvoice.orderStatus);
    } else {
      this.statusOfInvoice = null;
      this.modalRef.hide();
      this.loader = false;
    }
  }
}

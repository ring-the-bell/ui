import { Component, OnInit, TemplateRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';

import { ProductsService } from './products.service';
import { UtilsService } from '../shared/utils.service';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TitleCasePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  modalRef: BsModalRef;
  productForm: FormGroup;

  public nameMinLength = 2;
  public nameMaxLength = 30;
  public productsArr = [];
  public categoriesArr;
  public action;
  public fileErr = null;
  public fldComboErr = null;
  uploaded = false;
  imageUrl;
  productFile;
  public product;
  public statusArr = [{ id: 0, label: 'Active' }, { id: 1, label: 'Inactive' }];
  public categorySearch = '';
  public openSearch = '';
  public pagination = {
    itemsPerPage: 3,
    currentPage: 1,
    totalItems: 0
  }
  public loader;

  public prodTypes = [{
    name: 'grms',
    label: 'Grms'
  }, {
    name: 'ml',
    label: 'ML'
  }];

  public prodSearch;

  public reqMsgErr = 'This is a required field';

  constructor(
    private domSanitizer: DomSanitizer,
    private service: ProductsService,
    private modalService: BsModalService,
    private utils: UtilsService,
    private fb: FormBuilder,
    private titlecasePipe: TitleCasePipe,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.formValidations();
    this.getCategories();
    this.getAllProducts();
  }

  /**
   * TODO: comment formValidations
   * @description Forms validations
   * @author (Siva Sankar)
   */
  formValidations() {
    this.productForm = this.fb.group({
      productCategoryId: ['', Validators.required],
      productName: ['', Validators.compose([Validators.required, Validators.minLength(this.nameMinLength), Validators.maxLength(this.nameMaxLength), Validators.pattern(/^[a-zA-Z ]*$/)])],
      productPrice: ['', Validators.compose([Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.min(0)])],
      productOffer: ['', Validators.min(0)],
      productPackWeight: ['', Validators.compose([Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.min(1)])],
      productPackType: ['', Validators.required],
      productPackStock: ['', Validators.compose([Validators.required, Validators.pattern(/^-?(0|[1-9]\d*)?$/), Validators.min(1)])],
      productStatus: [0, Validators.required]
    })
  }

  /**
   * TODO: comment frmCtrls
   * @description Gets frm ctrls
   */
  get frmCtrls() {
    return this.productForm.controls;
  }

  /**
   * TODO: comment getAllProducts
   * @description Gets all products
   * @author (Siva Sankar)
   */
  getAllProducts() {
    this.prodSearch = null;
    this.loader = true;
    this.service.getAllProducts().subscribe(
      resp => { console.log(resp); 
        if (resp['data'] && resp['data'].length > 0) {
          this.productsArr = resp['data'];  
        } else {
          this.productsArr = [];
        }
        this.loader = false;
      },
      error => { console.log(error); }
    );
  }

  /**
   * TODO: comment getCategories
   * @description Gets categories
   * @author (Siva Sankar)
   */
  getCategories() {
    this.service.getCategories().subscribe(
      resp => { console.log(resp); this.categoriesArr = resp['data'] },
      error => { console.log(error); }
    );
  }

  /**
   * TODO: comment getCatName
   * @description Gets cat name
   * @author (Siva Sankar)
   * @param catId 
   * @returns  
   */
  getCatName(catId) {
    if (catId) {
      if (this.categoriesArr && this.categoriesArr.length > 0) {
        let cat = this.categoriesArr.find(obj => obj.categoryId == catId);
        return cat.categoryName;
      } else {
        return catId;
      }
    } else {
      return catId;
    }
  }

  /**
   * TODO: comment openModal
   * @description Opens modal
   * @author (Siva Sankar)
   * @param template 
   * @param type 
   * @param [data] 
   */
  openModal(template: TemplateRef<any>, type, data?) {
    this.fileErr = false;
    this.action = type;
    this.product = null;
    this.fldComboErr = null;
    this.productFile = null;
    this.productForm.reset();
    this.productForm.patchValue({ productCategoryId: '' });
    this.productForm.patchValue({ productStatus: '' });
    let modalConfig = {
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'modal-dialog-centered checkout-auth-modal-cls'
    };

    if (type) {
      if (type == 'add') {
        this.imageUrl = 'assets/images/logo/image_placeholder.jpg';
      } else if (type == 'update' && data) {
        console.log(data);
        let obj = {
          productCategoryId: data.productCategoryId ? data.productCategoryId : '',
          productName: data.productName ? data.productName : '',
          productPrice: data.productPrice ? data.productPrice : '',
          productOffer: data.productOffer ? data.productOffer : '',
          productPackWeight: data.productPackWeight ? data.productPackWeight : '',
          productPackType: data.productPackType ? data.productPackType : '',
          productPackStock: data.productPackStock ? data.productPackStock : '',
          productStatus: data.productStatus ? data.productStatus : '',
        }
        if (data.productImage) {
          this.imageUrl = data.productImage;
          this.uploaded = true;
          this.fileErr = false;
        }
        this.productForm.setValue(obj);
        this.product = data;
      }
      this.modalRef = this.modalService.show(template, modalConfig);
    }
  }

  /**
   * TODO: comment fileUpload
   * @description Files upload
   * @author (Siva Sankar)
   * @param event 
   */
  fileUpload(event) {
    this.loader = true;
    this.uploaded = true;
    let reader = new FileReader();
    console.log(event.target.files[0]);
    if (event.target.files && event.target.files.length > 0) {
      this.loader = true;
      let file = event.target.files[0];
      let ext = file.name.substring(file.name.lastIndexOf('.') + 1);
      if (ext.toLowerCase() == 'png' || ext.toLowerCase() == 'jpeg' || ext.toLowerCase() == 'jpg') {
        this.productFile = file;
        let img = new Image();
        img.src = window.URL.createObjectURL(file);
        reader.readAsDataURL(file);
        reader.onload = () => {
          setTimeout(() => {
            const width = img.naturalWidth;
            const height = img.naturalHeight;
  
            window.URL.revokeObjectURL(img.src);
            // console.log(width + '*' + height, reader.result);
            if (width < 320 && height < 320) {
              this.fileErr = "Size should be greater than 320*320";
              this.loader = false;
            } else {
              this.imageUrl = reader.result as string;
              this.fileErr = null;
              this.loader = false;
            }
          }, 2000);
        };
      } else {
        this.uploaded = false;
        this.productFile = null;
        this.loader = false;
        this.toastr.error("Only jpeg/jpg, and png files are allowed.");
      }
    } else {
      this.imageUrl = 'assets/images/logo/image_placeholder.jpg';
      this.uploaded = false;
      this.productFile = null;
      this.loader = false;
    }
  }

  /**
   * TODO: comment uploadProduct
   * @description Uploads product
   * @author (Siva Sankar)
   * @param action 
   */
  uploadProduct(action) {
    this.loader = true;
    if (this.productForm.valid) {
      if (!this.uploaded) {
        this.loader = false;
        this.fileErr = "product Image is required";
      } else if (this.productForm.value.productStatus == 1 && action != 'add') {
        console.log(this.productForm.value.productStatus);
        this.service.checkProduct(this.product.productId).subscribe(
          resp => {
            this.changeToReqObj(action);
          },err => {
            this.loader = false;
            this.toastr.error(err.error.message);
          }
        );
      } else {
        if (!this.fileErr) {
          // this.ngxLoader.start();
          console.log(this.productForm.value);
          this.changeToReqObj(action);
        } else {
          this.loader = false;
        }
      }
    }
  }

  /**
   * TODO: comment changeToReqObj
   * @description Changes to req obj
   * @author (Siva Sankar)
   * @param action 
   */
  changeToReqObj(action) {
    const formData = new FormData();
      formData.append('image', this.productFile);
      formData.append('productCategoryId', this.productForm.value.productCategoryId);
      formData.append('productName', this.productForm.value.productName);
      formData.append('productPrice', this.productForm.value.productPrice);
      formData.append('productOffer', this.productForm.value.productOffer);
      formData.append('productPackWeight', this.productForm.value.productPackWeight);
      formData.append('productPackType', this.productForm.value.productPackType);
      formData.append('productPackStock', this.productForm.value.productPackStock);
      // formData.append('productImage', this.imageUrl);
      formData.append('productStatus', this.productForm.value.productStatus);
      if (action.toLowerCase() == 'add') {
        this.addProduct(formData);
      } else {
        this.updateProduct(formData);
      }
  }

  /**
   * TODO: comment addProduct
   * @description Adds product
   * @author (Siva Sankar)
   * @param reqObj 
   */
  addProduct(reqObj) {
    this.service.addProduct(reqObj).subscribe(
      resp => {
        console.log(resp);
        if (resp) {
          this.successFnc(resp);
        } else {
          this.successFnc();
        }
      },
      error => {
        console.log(error);
        if (error) {
          this.errorFnc(error);
        } else {
          this.errorFnc();
        }
      }
    );
  }

  /**
   * TODO: comment updateProduct
   * @description Updates product
   * @author (Siva Sankar)
   * @param reqObj 
   */
  updateProduct(reqObj) {
    this.loader = true;
    this.service.updateProduct(reqObj, this.product.productId).subscribe(
      resp => {
        this.loader = false;
        if (resp) {
          this.successFnc(resp);
        } else {
          this.successFnc();
        }
      },
      error => {
        console.log(error);
        if (error) {
          this.errorFnc(error);
        } else {
          this.errorFnc();
        }
      }
    );
  }

  /**
   * TODO: comment successFnc
   * @description Success fnc
   * @author (Siva Sankar)
   * @param [resp] 
   */
  successFnc(resp?) {
    if (resp) {
      this.prodSearch = null;
      this.loader = false;      
      this.modalRef.hide();
      this.productForm.reset();
      this.toastr.success(resp['message']);
      this.getAllProducts();
    } 
  }

  /**
   * TODO: comment errorFnc
   * @description Errors fnc
   * @author (Siva Sankar)
   * @param [err] 
   */
  errorFnc(err?) {
    console.log(err);
    this.loader = false;
    if (err) {
      if (err.error.status_code == 409) {
        this.fldComboErr = err.error.message;
      } else {
        this.modalRef.hide();
        this.toastr.error(err.error['message']);
        this.getAllProducts();
      }
    } else {
      this.modalRef.hide();
      this.toastr.error("Something went wrong.");
      this.getAllProducts();
    }
  }

  /**
   * TODO: comment pageChange
   * @description Pages change
   * @author (Siva Sankar)
   * @param e 
   */
  pageChange(e) {
    console.log(e);
  }

  /**
   * TODO: comment searchProduct
   * @description Searchs product
   * @author (Siva Sankar)
   */
  searchProduct() {
    if (this.prodSearch) {
      this.service.searchProduct(this.prodSearch).subscribe(
        resp => {
          if (resp['data'] && resp['data'].length > 0) {
            this.productsArr = resp['data'];  
          } else {
            this.productsArr = [];
          }
          this.loader = false;
        },err => {
          this.loader = false;
        }
      );
    }
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment getAllProducts
   * @description Gets all products
   * @author (Siva Sankar)
   * @returns  
   */
  getAllProducts() {
    return this.httpClient.get(`${this.APIURL}store/products`).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment getCategories
   * @description Gets categories
   * @author (Siva Sankar)
   * @returns  
   */
  getCategories() {
    return this.httpClient.get(`${this.APIURL}categories/`).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment addProduct
   * @description Adds product
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  addProduct(reqObj) {
    return this.httpClient.post(`${this.APIURL}product/new`, reqObj).pipe(
      map(resp => resp)
    );
  }

/**
 * TODO: comment updateProduct
 * @description Updates product
 * @author (Siva Sankar)
 * @param reqObj 
 * @param productId 
 * @returns  
 */
updateProduct(reqObj, productId) {
    return this.httpClient.post(`${this.APIURL}product/update/${productId}`, reqObj).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment checkProduct
   * @description Checks product
   * @author (Siva Sankar)
   * @param productId 
   * @returns  
   */
  checkProduct(productId) {
    return this.httpClient.get(`${this.APIURL}store/product/verify/${productId}`).pipe(
      map(resp => resp)
    );
  }

/**
 * TODO: comment searchProduct
 * @description Searchs product
 * @author (Siva Sankar)
 * @param prodName 
 * @returns  
 */
searchProduct(prodName) {
    return this.httpClient.get(`${this.APIURL}store/product/search/${prodName}`).pipe(
      map(resp => resp)
    );
  }


}

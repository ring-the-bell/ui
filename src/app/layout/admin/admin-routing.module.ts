import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProductsComponent } from './products/products.component';
import { OrdersComponent } from './orders/orders.component';
import { AuthGuard } from 'src/app/shared/auth-guard.service';
import { RoleGuard } from 'src/app/shared/role-guard.service';
import { StoreDetailsComponent } from './store-details/store-details.component';

const routes: Routes = [{
  path: '',
  component: AdminComponent,
  canActivateChild: [RoleGuard],
  children: [{
    path: '',
    redirectTo: 'profile',
    pathMatch: 'full',
    data: { 
      expectedRole: ['retailer']
    }
  }, {
    path: 'dashboard',
    component: DashboardComponent,
    data: { 
      expectedRole: ['retailer'],
      title: 'Dashboard'
    }
  }, {
    path: 'products',
    component: ProductsComponent,
    data: { 
      expectedRole: ['retailer'],
      title: 'Products'
    }
  }, {
    path: 'orders',
    component: OrdersComponent,
    data: { 
      expectedRole: ['retailer'],
      title: 'Orders'
    }
  }, {
    path: 'profile',
    component: StoreDetailsComponent,
    data: { 
      expectedRole: ['retailer'],
      title: 'Profile'
    }
  }]
},
{path: '**', redirectTo: 'products'}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

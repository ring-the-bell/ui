import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { TitleCasePipe } from "@angular/common";

import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";

import { LoginService } from "./login.service";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthService } from "../shared/auth.service";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  modalRef: BsModalRef;

  public nameMinLength = 2;
  public nameMaxLength = 15;
  public pwdMinLength = 6;
  public pwdMaxLength = 15;

  public reqMsg = "This is a required field";
  public contentObj;
  public loader;
  public returnUrl;

  modalConfig = {
    animated: true,
    keyboard: true,
    backdrop: true,
    ignoreBackdropClick: true,
    class: "modal-sm modal-dialog-centered custom-modal-cls"
  };

  @ViewChild("responseModal", { static: true }) responseModal: TemplateRef<any>;

  constructor(
    private fb: FormBuilder,
    private service: LoginService,
    private router: Router,
    private modalService: BsModalService,
    private titlecasePipe: TitleCasePipe,
    private authService: AuthService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private cookieService: CookieService
  ) {}

  ngOnInit() {
    this.loader = true;
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
    // this.route.queryParamMap.subscribe(qryParams => {
    //   console.log(qryParams);
    // });
    if (this.authService.isLoggedIn()) {
      let roleObj = JSON.parse(this.authService.getUserInfo());
      if (roleObj) {
        if (roleObj.userType.toLowerCase() == "retailer") {
          this.router.navigate(["/admin"]);
        } else {
          this.router.navigate(["/groceries/home"]);
        }
      }
    } else {
      this.formValidations();
      this.loader = false;
    }
  }

  /**
   * TODO: comment formValidations
   * @description Forms validations
   * @author (Siva Sankar)
   */
  formValidations() {
    this.loginForm = this.fb.group({
      username: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(this.nameMinLength),
          Validators.maxLength(this.nameMaxLength)
        ])
      ],
      password: [
        "",
        Validators.compose([
          Validators.required,
          Validators.minLength(this.pwdMinLength),
          Validators.maxLength(this.pwdMaxLength)
        ])
      ]
    });
  }

  /**
   * TODO: comment frmCtrls
   * @description Gets frm ctrls
   */
  get frmCtrls() {
    return this.loginForm.controls;
  }

  /**
   * TODO: comment onSubmit
   * @description Determines whether submit on
   * @author (Siva Sankar)
   */
  onSubmit() {
    if (this.loginForm.valid) {
      this.loader = true;
      this.service.login(this.loginForm.value).subscribe(
        resp => {
          if (resp) {
            this.successFnc(resp);
          } else {
            this.successFnc();
          }
        },
        error => {
          console.log(error);
          if (error) {
            this.errorFnc(error);
          } else {
            this.errorFnc();
          }
        }
      );
    }
  }

  /**
   * TODO: comment successFnc
   * @description Success fnc
   * @author (Siva Sankar)
   * @param [resp] 
   */
  successFnc(resp?) {
    console.log(resp);
    if (resp) {
      this.cookieService.set("accessToken", resp.data.access_token);
      this.cookieService.set("userInfo", JSON.stringify(resp.data.data));
      this.toastr.success(resp["message"]);
      this.loader = false;
      if (this.returnUrl == "/") {
        if (resp.data.data.userType.toLowerCase() == "retailer") {
          this.router.navigate(["/admin"]);
        } else if (resp["data"].data.userType.toLowerCase() == "shopper") {
          this.router.navigate(["/groceries/home"]);
        } else {
          this.authService.logout();
        }
      } else {
        this.router.navigateByUrl(this.returnUrl);
      }
    } else {
      this.loader = false;
    }
  }

  /**
   * TODO: comment errorFnc
   * @description Errors fnc
   * @author (Siva Sankar)
   * @param [err] 
   */
  errorFnc(err?) {
    if (err) {
      console.log(err);
      if (err.error.status_code == 409) {
      } else {
        this.loader = false;
        this.toastr.error(err.error["message"]);
      }
    } else {
      this.loader = false;
      this.toastr.error("Something went wrong.");
    }
  }

  /**
   * TODO: comment openModal
   * @description Opens modal
   * @author (Siva Sankar)
   * @param template 
   */
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template, this.modalConfig);
  }

  /**
   * TODO: comment getClsBtnEvnt
   * @description Gets cls btn evnt
   * @author (Siva Sankar)
   * @param e 
   */
  getClsBtnEvnt(e) {
    console.log(e);
    this.modalRef.hide();
  }

  /**
   * TODO: comment goToSignup
   * @description Go to signup
   * @author (Siva Sankar)
   */
  goToSignup() {
    if (this.returnUrl == "/") {
      this.router.navigate(["/register"]);
    } else {
      this.router.navigate(["/register"], {
        queryParams: { returnUrl: this.returnUrl }
      });
    }
  }
}

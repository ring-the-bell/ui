import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment login
   * @description Logins login service
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  login(reqObj) {
    return this.httpClient.post(`${this.APIURL}auth/`, reqObj, { headers: environment.headers }).pipe(
      map(resp => resp)
    );
  }
}

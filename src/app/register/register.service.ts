import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment getAllStates
   * @description Gets all states
   * @author (Siva Sankar)
   * @returns  
   */
  getAllStates() {
    return this.httpClient.get(`${this.APIURL}states`).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment getAllCities
   * @description Gets all cities
   * @author (Siva Sankar)
   * @param stateId 
   * @returns  
   */
  getAllCities(stateId) {
    return this.httpClient.get(`${this.APIURL}cities/${stateId}`, { headers: environment.headers }).pipe(
      map(resp => resp)
    );
  }
  
  /**
   * TODO: comment verifyRecors
   * @description Verifys recors
   * @author (Siva Sankar)
   * @param type 
   * @param val 
   * @returns  
   */
  verifyRecors(type, val) {
    if (type) {
      if (type == 'storeName') {
        return this.httpClient.get(`${this.APIURL}verify/storename/${val}`).pipe(
          map(resp => resp)
        );
      } else if (type == 'name') {
        return this.httpClient.get(`${this.APIURL}verify/username/${val}`).pipe(
          map(resp => resp)
        );
      } else if (type == 'number') {
        return this.httpClient.get(`${this.APIURL}verify/number/${val}`).pipe(
          map(resp => resp)
        );
      } else if (type == 'email') {
        return this.httpClient.get(`${this.APIURL}verify/email/${val}`).pipe(
          map(resp => resp)
        );
      }
    }
  }

  /**
   * TODO: comment addStore
   * @description Adds store
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  addStore(reqObj) {
    return this.httpClient.post(`${this.APIURL}user/store/`, reqObj).pipe(
      map(resp => resp)
    );
  }

  /**
   * TODO: comment addCustomer
   * @description Adds customer
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  addCustomer(reqObj) {
    return this.httpClient.post(`${this.APIURL}user/customer/`, reqObj).pipe(
      map(resp => resp)
    );
  }
}

import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TitleCasePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { CookieService } from 'ngx-cookie-service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { RegisterService } from './register.service';
import { AuthService } from '../shared/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  modalRef: BsModalRef;
  modalConfig = {
    animated: true,
    keyboard: false,
    backdrop: true,
    ignoreBackdropClick: true,
    class: ''
  };
  storeRegForm: FormGroup;
  customerRegForm: FormGroup;

  public nameMinLength = 2;
  public nameMaxLength = 15;
  public pwdMinLength = 6;
  public pwdMaxLength = 15;

  public isStore = true;
  public reqMsg = "This is a required field";
  public storePwdErr = null;
  public customerPwdErr = null;
  public fileErr = null;
  public uploaded = false;
  public imgBase64 = 'assets/images/placeholder.jpg';
  public storeLogo = null;
  public loader = false;
  public returnUrl;

  public statesList;
  public citiesList;
  public isDeliveryInfoActive = false;
  public menuArr = [{id : 1, name: 'Store', class: 'active'}, {id: 2, name: 'Customer', class: ''}];
  public locationObj = {
    isEnabled: false,
    lat: null,
    lang: null
  };

  @ViewChild('locationModal', {static: true}) locationModal: TemplateRef<any>;

  constructor(private fb: FormBuilder, private service: RegisterService, private titlecasePipe:TitleCasePipe, private authService: AuthService, private router: Router, private route: ActivatedRoute, private toastr: ToastrService, private modalService: BsModalService, private cookieService: CookieService) { }

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    
    if (this.authService.isLoggedIn()) {
      let roleObj = JSON.parse(this.authService.getUserInfo());
      if (roleObj) {
        if (roleObj.userType.toLowerCase() == 'retailer') {
          this.router.navigate(['/admin']);
        } else {
          this.router.navigate(['/groceries/home']);
        }
      }      
    } else {
      // this.getLocation();
      this.loader = true;
      this.storeFormValidation();
      this.customerFormValidation();
      this.getAllStates();
      this.loader = false;
    }
    // this.route.queryParamMap.subscribe(qryParams => {
    //   console.log(qryParams);
    // });
  }

  /**
   * TODO: comment getLocation
   * @description Gets location
   * @author (Siva Sankar)
   */
  getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            // Success function
            this.showPosition.bind(this), 
            // Error function
            this.showError.bind(this), 
            // Options. See MDN for details.
            {
               enableHighAccuracy: true,
               timeout: 5000,
               maximumAge: 0
            });
    } else { 
        console.log("Geolocation is not supported by this browser.");
    }
}

 /**
  * TODO: comment showPosition
  * @description Shows position
  * @author (Siva Sankar)
  * @param position 
  */
 showPosition(position) {
   this.locationObj.isEnabled = true;
   this.locationObj.lat = position.coords.latitude;
   this.locationObj.lang = position.coords.longitude;
   console.log(position.coords.latitude, position.coords.longitude);
}

 /**
  * TODO: comment showError
  * @description Shows error
  * @author (Siva Sankar)
  * @param error 
  */
 showError(error) {
   console.log(this.locationObj);
  switch(error.code) {
    case error.PERMISSION_DENIED:
      this.locationObj.isEnabled = false;
      this.locationObj.lat = this.locationObj.lang = null;
      this.modalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
      this.modalRef = this.modalService.show(this.locationModal, this.modalConfig);
      console.log("User denied the request for Geolocation.");
      break;
    case error.POSITION_UNAVAILABLE:
      this.locationObj.isEnabled = false;
      this.locationObj.lat = this.locationObj.lang = null;
      this.modalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
      this.modalRef = this.modalService.show(this.locationModal, this.modalConfig);
      console.log("Location information is unavailable.");
      break;
    case error.TIMEOUT:
      this.locationObj.isEnabled = false;
      this.locationObj.lat = this.locationObj.lang = null;
      this.modalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
      this.modalRef = this.modalService.show(this.locationModal, this.modalConfig);
      console.log("The request to get user location timed out.");
      break;
    case error.UNKNOWN_ERROR:
      this.locationObj.isEnabled = false;
      this.locationObj.lat = this.locationObj.lang = null;
      this.modalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
      this.modalRef = this.modalService.show(this.locationModal, this.modalConfig);
      console.log("An unknown error occurred.");
      break;
  }
}

  /**
   * TODO: comment storeFormValidation
   * @description Stores form validation
   * @author (Siva Sankar)
   */
  storeFormValidation() {
    this.storeRegForm = this.fb.group({
      storeName: ['', Validators.compose([Validators.required, Validators.minLength(this.nameMinLength), Validators.maxLength(this.nameMaxLength)])],
      email: ['', Validators.compose([Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")])],
      address: ['', Validators.compose([Validators.required])],
      fullName: ['', Validators.compose([Validators.required, Validators.minLength(this.nameMinLength), Validators.maxLength(this.nameMaxLength)])],
      mobileNumber: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(15), Validators.pattern("^[0-9 ]*$")])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(this.pwdMinLength), Validators.maxLength(this.pwdMaxLength)])],
      confirmPassword: ['', Validators.compose([Validators.required, Validators.minLength(this.pwdMinLength), Validators.maxLength(this.pwdMaxLength)])],
      state: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])],
      delivery: [''],
      amount: [''],
      charges: ['']
    });
  }

  /**
   * TODO: comment customerFormValidation
   * @description Customers form validation
   * @author (Siva Sankar)
   */
  customerFormValidation() {
    this.customerRegForm = this.fb.group({
      fullName: ['', Validators.compose([Validators.required, Validators.minLength(this.nameMinLength), Validators.maxLength(this.nameMaxLength)])],
      email: ['', Validators.compose([Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")])],
      address: ['', Validators.compose([Validators.required])],
      mobileNumber: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(15), Validators.pattern("^[0-9 ]*$")])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(this.pwdMinLength), Validators.maxLength(this.pwdMaxLength)])],
      confirmPassword: ['', Validators.compose([Validators.required, Validators.minLength(this.pwdMinLength), Validators.maxLength(this.pwdMaxLength)])],
      state: ['', Validators.compose([Validators.required])],
      city: ['', Validators.compose([Validators.required])]
    });
  }

  /**
   * TODO: comment storeFrmCtrls
   * @description Gets store frm ctrls
   */
  get storeFrmCtrls() {
    return this.storeRegForm.controls;
  }

  /**
   * TODO: comment customerFrmCtrls
   * @description Gets customer frm ctrls
   */
  get customerFrmCtrls() {
    return this.customerRegForm.controls;
  }


  /**
   * TODO: comment changeMenu
   * @description Changes menu
   * @author (Siva Sankar)
   * @param menu 
   */
  changeMenu(menu) {
    console.log(menu);
    this.menuArr.filter(obj => {
      if (obj.id == menu.id) {
        obj.class = 'active';
      } else {
        obj.class = '';
      }
    });

    if (menu.id == 1) {
      this.customerRegForm.reset();
      this.isStore = true;
    } else {
      this.isDeliveryInfoActive = false;
      this.storeRegForm.reset();
      this.fileErr = null;
      this.uploaded = false;
      this.storeLogo = null;
      this.isStore = false;
    }
  }

  /**
   * TODO: comment checkRecord
   * @description Checks record
   * @author (Siva Sankar)
   * @param val 
   * @param type 
   */
  checkRecord(val, type) {
    console.log(val, type);
    if (val && type) {
      let fieldVal;
      if (val == 'storeName') {
        if (type == 'store') {
          if (this.storeRegForm.value.storeName) {
            this.loader = true;
            fieldVal = this.storeRegForm.value.storeName;
            this.verifyRecors(val, fieldVal);
          }
        }
      } else if (val == 'name') {
        if (type == 'store') {
          if (this.storeRegForm.value.fullName) {
            this.loader = true;
            fieldVal = this.storeRegForm.value.fullName;
            this.verifyRecors(val, fieldVal);
          }
        } else if (type == 'customer') {
          if (this.customerRegForm.value.fullName) {
            this.loader = true;
            fieldVal = this.customerRegForm.value.fullName;
            this.verifyRecors(val, fieldVal);
          }
        }
        this.loader = true;
        fieldVal = this.storeRegForm.value.fullName;
        this.verifyRecors(val, fieldVal);
      } else if (val == 'number') {
        if (type == 'store') {
          if (this.storeRegForm.value.mobileNumber) {
            this.loader = true;
            fieldVal = this.storeRegForm.value.mobileNumber;
            this.verifyRecors(val, fieldVal);
          }
        } else if (type == 'customer') {
          if (this.customerRegForm.value.mobileNumber) {
            this.loader = true;
            fieldVal = this.customerRegForm.value.mobileNumber;
            this.verifyRecors(val, fieldVal);
          }
        }
        fieldVal = this.storeRegForm.value.mobileNumber;
        this.verifyRecors(val, fieldVal);
      } else if (val == 'email') {
        if (type == 'store') {
          if (this.storeRegForm.value.email) {
            this.loader = true;
            fieldVal = this.storeRegForm.value.email;
            this.verifyRecors(val, fieldVal);
          }
        } else if (type == 'customer') {
          if (this.customerRegForm.value.email) {
            this.loader = true;
            fieldVal = this.customerRegForm.value.email;
            this.verifyRecors(val, fieldVal);
          }
        }
      }
    }
  }
  
  /**
   * TODO: comment verifyRecors
   * @description Verifys recors
   * @author (Siva Sankar)
   * @param val 
   * @param fieldVal 
   */
  verifyRecors(val, fieldVal) {
    if (val && fieldVal) {
      this.service.verifyRecors(val, fieldVal).subscribe(
        resp => {
          this.loader = false;
        },
        err => {
          console.log(err);
          if (err) {
            this.loader = false;
            this.toastr.error(err.error.message);
          } else {
            this.toastr.error(err.error.message);
          }
        }
      );
    } else {
      this.loader = false;
    }
  }


  /**
   * TODO: comment getDeliveryInfo
   * @description Gets delivery info
   * @author (Siva Sankar)
   */
  getDeliveryInfo() {
    console.log(this.storeRegForm.value);
    if (this.storeRegForm.value.delivery) {
      this.isDeliveryInfoActive = true;
      this.storeRegForm.get('amount').setValidators([Validators.compose([Validators.required, Validators.min(0)])]);
      this.storeRegForm.get('ew').setValidators([Validators.compose([Validators.required, Validators.min(0)])]);
    } else {
      this.isDeliveryInfoActive = false;
      this.storeRegForm.get('amount').clearValidators();
      this.storeRegForm.get('charges').clearValidators();
      this.storeRegForm.get('amount').setValue('');
      this.storeRegForm.get('charges').setValue('');
    }
  }

  /**
   * TODO: comment fileUpload
   * @description Files upload
   * @author (Siva Sankar)
   * @param event 
   */
  fileUpload(event) {
    this.loader = true;
    this.uploaded = true;
    let reader = new FileReader();
    console.log(event.target.files[0]);
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      let ext = file.name.substring(file.name.lastIndexOf('.') + 1);
      if (ext.toLowerCase() == 'png' || ext.toLowerCase() == 'jpeg' || ext.toLowerCase() == 'jpg') {
        this.storeLogo = file;
        let img = new Image();
        img.src = window.URL.createObjectURL(file);
        reader.readAsDataURL(file);
        reader.onload = () => {
          setTimeout(() => {
            const width = img.naturalWidth;
            const height = img.naturalHeight;
  
            window.URL.revokeObjectURL(img.src);
            this.imgBase64 = reader.result as string;
            this.fileErr = null;
            // console.log(width + '*' + height, reader.result);
            if (width < 320 && height < 320) {
              // this.fileErr = "Size should be greater than 320*320";
            } else {
              // console.log(reader.result);
              this.imgBase64 = reader.result as string;
              this.fileErr = null;
              // document.querySelector('#product-img').setAttribute('src', reader.result as string);
              // console.log(document.querySelector('#product-img'));
            }
            this.loader = false;
          }, 2000);
        };
      } else {
        this.uploaded = false;
        this.fileErr = null;
        this.storeLogo = null;
        this.loader = false;
        this.toastr.error("Only jpeg/jpg, and png files are allowed.");
      }
    } else {
      this.imgBase64 = 'assets/images/placeholder.jpg';
      this.uploaded = false;
      this.fileErr = null;
      this.storeLogo = null;
      this.loader = false;
    }
  }

  /**
   * TODO: comment getAllStates
   * @description Gets all states
   * @author (Siva Sankar)
   */
  getAllStates() {
    this.service.getAllStates().subscribe(
      resp => { console.log(resp); this.statesList = resp['data']; },
      error => { console.log(error); }
    );
  }

  /**
   * TODO: comment getAllCities
   * @description Gets all cities
   * @author (Siva Sankar)
   * @param stateId 
   */
  getAllCities(stateId) {
    this.loader = true;
    console.log(stateId);
    if (stateId && stateId > 0) {
      this.service.getAllCities(stateId).subscribe(
        resp => { console.log(resp); this.citiesList = resp['data']; this.loader = false; },
        error => { console.log(error); }
      );
    }
  }

  /**
   * TODO: comment onCustomerSubmit
   * @description Determines whether customer submit on
   * @author (Siva Sankar)
   * @param locationModal 
   */
  onCustomerSubmit(locationModal: TemplateRef<any>) {
    // if (this.locationObj.isEnabled) {
      this.loader = true;
      console.log(this.customerRegForm.valid, this.customerRegForm.value);
      if (this.customerRegForm.valid) {
        let formObj = this.customerRegForm.value;
        // console.log(formObj, formObj.password, formObj.confirmPassword)
        if (formObj.password != formObj.confirmPassword) {
          this.loader = false;
          this.customerPwdErr = 'Passwords should match';
        } else {
          this.customerPwdErr = null;
          delete formObj['confirmPassword'];
          // let fd = new FormData;
          // Object.entries(formObj).forEach(([key, value]) => {
          //   console.log(`${key}: ${value}`);
          //   fd.append(key, formObj[key]);
          // });
          formObj['userType'] = 'shopper';
          formObj['latitude'] = this.locationObj.lat;
          formObj['langitude'] = this.locationObj.lang;
          this.service.addCustomer(formObj).subscribe(
            resp => {
              this.loader = false;
              if (resp) {
                if (resp['status_code'] == 200) {
                  this.cookieService.set('accessToken', resp['data'].access_token);
                  this.cookieService.set('userInfo', JSON.stringify(resp['data'].data));
                  this.toastr.success(resp['message']);
                    if (this.returnUrl && this.returnUrl !== '/') {
                      this.router.navigateByUrl(this.returnUrl);
                    } else {
                      if (resp['data'].data.userType.toLowerCase() == 'retailer') {
                        this.router.navigate(['/admin']); 
                      } else {
                        this.router.navigate(['/groceries/home']);              
                      }
                    }
                } else {
                  this.toastr.error(resp['message']);
                }
              } else {
                this.toastr.error(resp['Something went wrong. pLease try agian later']);
              }
            },
            err => {
              this.loader = false;
              if (err) {
                this.toastr.error(err.error.message);
              } else {
                this.toastr.error(err.error.message);
              }
            }
          );
        }
      }
    // } else {
    //   this.modalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
    //   this.modalRef = this.modalService.show(locationModal, this.modalConfig);
    // }
  }

  /**
   * TODO: comment onStoreSubmit
   * @description Determines whether store submit on
   * @author (Siva Sankar)
   * @param template 
   * @param locationModal 
   */
  onStoreSubmit(template: TemplateRef<any>, locationModal: TemplateRef<any>) {  
    // if (this.locationObj.isEnabled) {
      this.loader = true;
      console.log(this.storeRegForm.valid, this.storeRegForm.value);
      if (this.storeRegForm.valid) {
        let formObj = this.storeRegForm.value;
        // console.log(formObj, formObj.password, formObj.confirmPassword)
        if (formObj.password != formObj.confirmPassword) {
          this.loader = false;
          this.storePwdErr = 'Paswwords should match';
        } else if (this.uploaded && this.fileErr == null) {
          this.storePwdErr = this.fileErr = null;
          delete formObj['confirmPassword'];
          let fd = new FormData;
          Object.entries(formObj).forEach(([key, value]) => {
            // console.log(`${key}: ${value}`);
            fd.append(key, formObj[key]);
          });
          fd.append('storeLogo', this.storeLogo);
          fd.append('userType', 'retailer');
          fd.append('latitude', this.locationObj.lat);
          fd.append('langitude', this.locationObj.lang);
          this.service.addStore(fd).subscribe(
            resp => {
              if (resp) {
                if (resp['status_code'] == 200) {
                  this.cookieService.set('accessToken', resp['data'].access_token);
                  this.cookieService.set('userInfo', JSON.stringify(resp['data'].data));
                  this.loader = false;
                  if (resp['data'].data.userType.toLowerCase() == 'retailer') {
                    this.modalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
                    this.modalRef = this.modalService.show(template, this.modalConfig);
                    setTimeout(() => {
                      this.router.navigate(['/admin']); 
                      this.modalRef.hide();
                    }, 3500);
                  } else if (resp['data'].data.userType.toLowerCase() == 'shopper') {
                    this.toastr.success(resp['message']);
                    this.router.navigate(['/groceries/home']);              
                  } else {
                    this.authService.logout();
                  }
                } else {
                  this.toastr.error(resp['message']);
                }
              } else {
                this.toastr.error("Something went wrong. pLease try agian later");
              }
            },
            error => {
              this.loader = false;
              if (error) {
                this.toastr.error(error.error.message);
              } else {
                this.toastr.error("Something went wrong. pLease try agian later");
              }
            }
          );
        } else {
          this.storePwdErr = null;
          this.fileErr = 'Logo is required';
          this.loader = false;
        }
      }
    // } else {
    //   this.modalConfig.class = "modal-dialog-centered checkout-auth-modal-cls"; 
    //   this.modalRef = this.modalService.show(locationModal, this.modalConfig);
    // }
  }

  /**
   * TODO: comment goToSignin
   * @description Go to signin
   * @author (Siva Sankar)
   */
  goToSignin() {
    if (this.returnUrl == '/') {
      this.router.navigate(['/register']);
    } else {
      this.router.navigate(['/register'], { queryParams: { returnUrl: this.returnUrl }});
    }
  }
}

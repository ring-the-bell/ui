import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthService } from './auth.service';
import { CookieService } from 'ngx-cookie-service';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private auth: AuthService, private router: Router, private cookieService: CookieService) {}

  /**
   * TODO: comment intercept
   * @description Intercepts token interceptor
   * @author (Siva Sankar)
   * @param request 
   * @param next 
   * @returns intercept 
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    request = request.clone({
      setHeaders: {
        Authorization: `Bearer ${this.auth.getJwtToken()}`
      }
    });
    
    return next.handle(request).pipe( tap(() => {},
      (err: any) => {
        if (err instanceof HttpErrorResponse) {
          if (err.status == 401) {
            this.cookieService.deleteAll();
            this.router.navigate(['/login']);
            return;
          } else if (err.status == 403) {
            this.router.navigateByUrl(this.router.url);
            return;
          }
        }
    }));
  }
}
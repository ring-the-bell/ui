import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";

import { environment } from "src/environments/environment";
@Injectable({
  providedIn: "root"
})
export class UtilsService {
  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) {}

  /**
   * TODO: comment getDimensions
   * @description Gets dimensions
   * @author (Siva Sankar)
   * @param distance 
   * @param lat 
   * @param lang 
   * @returns  
   */
  getDimensions(distance, lat, lang) {
    if (distance && lat && lang) {
      let coef = distance * 0.0000089;
      let obj;
      obj.latitude = lat + coef;
      obj.langitude = lang + coef / Math.cos(lat * 0.018);
      return { latitude: obj.latitude, langitude: obj.langitude };
    } else {
      return { latitude: lat, langitude: lang };
    }
  }
}

import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoleGuard implements CanActivate {

  constructor(
    public auth: AuthService,
    public router: Router
  ) { }

  /**
   * TODO: comment canActivate
   * @description Determines whether activate can
   * @author (Siva Sankar)
   * @param route 
   * @param state 
   * @returns activate 
   */
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        const expectedRole = route.data.expectedRole;
        if (this.auth.getUserInfo()) {
            const userData = JSON.parse(this.auth.getUserInfo());
            if (this.auth.isLoggedIn() && userData.userType == expectedRole) {
                return true;
            } else if (this.auth.isLoggedIn()) {
                if (expectedRole == 'shopper') {
                    this.router.navigate(['/groceries/home']);
                    return false;
                } else if (expectedRole == 'retailer') {
                    this.router.navigate(['/admin']);
                    return false;
                } else {
                    this.auth.logout();
                    this.router.navigate(['/login']);
                    return false;
                }
            } else {
                this.auth.logout();
                this.router.navigate(['/login']);
                return false;
            }
        } else {
            this.auth.logout();
            this.router.navigate(['/login']);
            return false;
        }
  }

  /**
   * TODO: comment canActivateChild
   * @description Determines whether activate child can
   * @author (Siva Sankar)
   * @param route 
   * @param state 
   * @returns  
   */
  canActivateChild( route : ActivatedRouteSnapshot, state : RouterStateSnapshot ) {
    const expectedRole = route.data.expectedRole;
    if (this.auth.getUserInfo()) {
        const userData = JSON.parse(this.auth.getUserInfo());
        if (this.auth.isLoggedIn() && userData.userType == expectedRole) {
            return true;
        } else if (this.auth.isLoggedIn()) {
            if (expectedRole == 'shopper') {
                this.router.navigate(['/groceries/home']);
                return false;
            } else if (expectedRole == 'retailer') {
                this.router.navigate(['/admin']);
                return false;
            } else {
                this.auth.logout();
                this.router.navigate(['/login']);
                return false;
            }
        } else {
            this.auth.logout();
            this.router.navigate(['/login']);
            return false;
        }
    } else {
        this.auth.logout();
        this.router.navigate(['/login']);
        return false;
    }
  }
}

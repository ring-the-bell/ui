import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private cookieService: CookieService) { }

  /**
   * TODO: comment isLoggedIn
   * @description Determines whether logged in is
   * @author (Siva Sankar)
   * @returns true if logged in 
   */
  isLoggedIn(): boolean {
    return this.getJwtToken() ? true : false;
  }

  /**
   * TODO: comment getUserInfo
   * @description Gets user info
   * @author (Siva Sankar)
   * @returns  
   */
  getUserInfo() {
    return this.cookieService.get('userInfo');
  }

  /**
   * TODO: comment getJwtToken
   * @description Gets jwt token
   * @author (Siva Sankar)
   * @returns  
   */
  getJwtToken() {
    return this.cookieService.get('accessToken');
  }

  /**
   * TODO: comment logout
   * @description Logouts auth service
   * @author (Siva Sankar)
   */
  logout() {
    this.cookieService.deleteAll();
  }

}

import { Component, OnInit } from "@angular/core";
import { Title, Meta } from "@angular/platform-browser";
import { Router, NavigationEnd, ActivatedRoute } from "@angular/router";

import { AppService } from "./app.service";

import { CookieService } from "ngx-cookie-service";
import { CanonicalService } from "./shared/canonical.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit {
  mainTitle = "Ring The Bell";
  isAlertVisiable = false;
  dispalyText = null;

  constructor(
    private router: Router,
    private titleService: Title,
    private activatedRoute: ActivatedRoute,
    private service: AppService,
    private cookieService: CookieService,
    private metaTagService: Meta,
    private canonicalService: CanonicalService
  ) {
    // this.metaTagService.addTags([
    //   {
    //     name: "keywords",
    //     content:
    //       "Online Groceries Store, Groceries Home Delivery, Online Groceires Stores Around You, Online Groceires Stores Near By You"
    //   },
    //   { name: "robots", content: "index, follow" },
    //   { name: "author", content: "SSR Groups" },
    //   {
    //     name: "description",
    //     content:
    //       "Shopping for grocery and everyday household products is an integral part of life. With changing time, consumers have started becoming technology savvy, resulting in the widespread use of the Internet and mobile phones. Our portal provides features such as comparative pricing, deals and coupons, home delivery options, and such others."
    //   }
    // ]);
    this.canonicalService.setCanonicalURL();
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        var title = this.getTitle(
          router.routerState,
          router.routerState.root
        ).join("-");
        // console.log('title', title);
        titleService.setTitle(`${this.mainTitle} | ${title}`);
      }
    });
  }

  ngOnInit() {
    setInterval(() => {
      this.checkDeployment();
    }, 18000000);
  }

  // collect that title data properties from all child routes
  // there might be a better way but this worked for me
  /**
   * TODO: comment getTitle
   * @description Gets title
   * @author (Siva Sankar)
   * @param state
   * @param parent
   * @returns
   */
  getTitle(state, parent) {
    var data = [];
    if (parent && parent.snapshot.data && parent.snapshot.data.title) {
      data.push(parent.snapshot.data.title);
    }

    if (state && parent) {
      data.push(...this.getTitle(state, state.firstChild(parent)));
    }
    return data;
  }
  // releaseDate: "2020-05-22 18:40:54"
  // versionNumber: "1.0"
  /**
   * TODO: comment checkDeployment
   * @description Checks deployment
   * @author (Siva Sankar)
   */
  checkDeployment() {
    this.service.getDeploymentDetails().subscribe(resp => {
      console.log(resp);
      if (resp) {
        if (resp["status_code"] == 200 && resp["data"]) {
          this.dispalyText = resp["data"].dispalyText;
          let daysCount = this.calculateDiff(resp["data"].releaseDate);
          // console.log(daysCount);
          if (typeof daysCount == "number") {
            if (daysCount <= 0) {
              if (
                !this.cookieService.get("isUpdated") ||
                this.cookieService.get("isUpdated") == "false"
              ) {
                this.isAlertVisiable = true;
              } else {
                this.isAlertVisiable = false;
              }
            }
          }
        }
      }
    });
  }

  /**
   * TODO: comment calculateDiff
   * @description Calculates diff
   * @author (Siva Sankar)
   * @param dateSent
   * @returns
   */
  calculateDiff(dateSent) {
    let currentDate = new Date();
    let cretedDate = new Date(dateSent);

    return Math.floor(
      (Date.UTC(
        currentDate.getFullYear(),
        currentDate.getMonth(),
        currentDate.getDate()
      ) -
        Date.UTC(
          cretedDate.getFullYear(),
          cretedDate.getMonth(),
          cretedDate.getDate()
        )) /
        (1000 * 60 * 60 * 24)
    );
  }

  /**
   * TODO: comment refreshThePage
   * @description Refreshs the page
   * @author (Siva Sankar)
   */
  refreshThePage() {
    window.location.reload(true);
    this.cookieService.set("isUpdated", "true");
  }
}

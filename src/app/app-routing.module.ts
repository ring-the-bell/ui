import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
const routes: Routes = [
  {
    path: "",
    redirectTo: "groceries",
    pathMatch: "full"
  },
  {
    path: "groceries",
    loadChildren: () =>
      import("./layout/user/user.module").then(module => module.UserModule)
  },
  {
    path: "admin",
    loadChildren: () =>
      import("./layout/admin/admin.module").then(module => module.AdminModule)
  },
  {
    path: "login",
    component: LoginComponent,
    data: { title: "Login" }
  },
  {
    path: "register",
    component: RegisterComponent,
    data: { title: "Register" }
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: "top",
      useHash: true,
      enableTracing: false
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

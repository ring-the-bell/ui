import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AppService {

  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment getDeploymentDetails
   * @description Gets deployment details
   * @author (Siva Sankar)
   * @returns  
   */
  getDeploymentDetails() {
    return this.httpClient.get(`${this.APIURL}site/updates`).pipe(
      map(resp => resp)
    );
  }
}

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-static',
  templateUrl: './static.component.html',
  styleUrls: ['./static.component.scss']
})
export class StaticComponent implements OnInit {

  @Input() contentObj

  @Output() clsBtnOpt = new EventEmitter;

  constructor() { }

  ngOnInit() {
  }

  modalCls() {
    this.clsBtnOpt.emit('close');
  }
}

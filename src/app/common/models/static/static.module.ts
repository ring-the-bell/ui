import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaticComponent } from './static.component';


@NgModule({
  declarations: [StaticComponent],
  imports: [
    CommonModule,
  ],
  exports:[StaticComponent]
})
export class StaticModule { }

import { Component, OnInit } from "@angular/core";
import { TitleCasePipe } from "@angular/common";
import { Router, ActivatedRoute } from "@angular/router";
import { HeaderService } from "./header.service";
import { AuthService } from "src/app/shared/auth.service";

import { ToastrService } from "ngx-toastr";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  isNavbarCollapse = false;
  cartAmount = 0;
  cartProducts;
  cartCount = 0;
  loggedUser = false;
  loader = false;

  constructor(
    private router: Router,
    private service: HeaderService,
    private authService: AuthService,
    private titlecasePipe: TitleCasePipe,
    private toastr: ToastrService,
    private cookieService: CookieService
  ) {}

  ngOnInit() {
    this.isLogin();
    this.getCartItems();
    this.service.updateCartItems.subscribe(data => {
      // console.log(data);
      this.getCartItems();
      // this.cartProducts = data;
    });
  }

  /**
   * TODO: comment isLogin
   * @description Determines whether login is
   * @author (Siva Sankar)
   */
  isLogin() {
    if (this.authService.isLoggedIn()) {
      this.loggedUser = true;
    } else {
      this.loggedUser = false;
    }
  }

  /**
   * TODO: comment logout
   * @description Logouts 
   * @author (Siva Sankar)
   */
  logout() {
    this.loader = true;
    this.getCartItems();
    setTimeout(() => {
      this.authService.logout();
      this.loggedUser = null;
      this.toastr.success("Logout successfully.");
      this.loader = false;
      this.router.navigate(["/groceries/home"]);
    }, 1000);
  }

  /**
   * TODO: comment toggleNavbar
   * @description Toggles navbar
   * @author (Siva Sankar)
   */
  toggleNavbar() {
    this.isNavbarCollapse = !this.isNavbarCollapse;
  }

  /**
   * TODO: comment getCartItems
   * @description Gets cart items
   * @author (Siva Sankar)
   */
  getCartItems() {
    if (this.authService.isLoggedIn()) {
      this.getUserItems();
    } else {
      this.authService.logout();
    }
  }

  /**
   * TODO: comment getUserItems
   * @description Gets user items
   * @author (Siva Sankar)
   */
  getUserItems() {
    let url = "cart/items/";
    this.service.getCartItems(url).subscribe(
      resp => {
        this.getItems(resp);
      },
      error => {
        console.log(error);
      }
    );
  }

  /**
   * TODO: comment getItems
   * @description Gets items
   * @author (Siva Sankar)
   * @param data 
   */
  getItems(data) {
    if (data && data["data"] && data["status_code"] == 200) {
      this.cookieService.set(
        "currentStore",
        JSON.stringify({ storeId: data["data"][0].storeId })
      );
      this.cartCount = data["data"].length;
      this.cartAmount = data["data"].reduce(function(prev, cur) {
        return (
          prev +
          parseFloat(cur.productOfferPrice) * parseFloat(cur.productQuantity)
        );
      }, 0);
      this.cartProducts = data["data"];
    } else {
      this.cartProducts = [];
      this.cartCount = this.cartAmount = 0;
    }
  }

  /**
   * TODO: comment goTocheckOut
   * @description Go tocheck out
   * @author (Siva Sankar)
   */
  goTocheckOut() {
    this.router.navigate(["/checkout"]);
  }
}

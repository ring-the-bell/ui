import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  public APIURL = environment.apiURL;

  public updateCartItems: BehaviorSubject<string> = new BehaviorSubject('');

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment getCartItems
   * @description Gets cart items
   * @author (Siva Sankar)
   * @param url 
   * @returns  
   */
  getCartItems(url){
    let reqUrl = `${this.APIURL}${url}`
    return this.httpClient.get(reqUrl).pipe(
      map(resp => resp)
    );
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ContactUsService {
  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  /**
   * TODO: comment sendContactInfo
   * @description Sends contact info
   * @author (Siva Sankar)
   * @param reqObj 
   * @returns  
   */
  sendContactInfo(reqObj) {
    return this.httpClient.post(`${this.APIURL}user/suggestions/`, reqObj).pipe(
      map(resp => resp)
    );
  }
}

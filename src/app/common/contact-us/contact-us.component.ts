import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
contactFrm: FormGroup;
public nameMinLength = 2;
  public nameMaxLength = 15;
  public reqMsg = "This is a required field";
  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.frmValidations();
  }

  /**
   * TODO: comment frmValidations
   * @description Frms validations
   * @author (Siva Sankar)
   */
  frmValidations() {
    this.contactFrm = this.fb.group({
      fullName: ['', Validators.compose([Validators.required, Validators.minLength(this.nameMinLength), Validators.maxLength(this.nameMaxLength)])],
      mobileNumber: ['', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(15), Validators.pattern("^[0-9 ]*$")])],
      email: ['', Validators.compose([Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")])],
      reason: ['', Validators.required]
    });
  }

  /**
   * TODO: comment frmCtrls
   * @description Gets frm ctrls
   */
  get frmCtrls() {
    return this.contactFrm.controls;
  }

  onSubmit() {

  }

}

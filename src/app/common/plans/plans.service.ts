import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class PlansService {
  public APIURL = environment.apiURL;

  constructor(private httpClient: HttpClient) { }

  getPlansDetails(){
    return this.httpClient.get(`${this.APIURL}site/subscription/plans`).pipe(
      map(resp => resp)
    );
  }
}

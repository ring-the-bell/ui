import { Component, OnInit, Input } from '@angular/core';
import { PlansService } from './plans.service';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {
@Input() isBtnShow;
plansList;
loader;
  constructor(private service: PlansService) { }

  ngOnInit() {
    this.getPlansDetails();
  }

  getPlansDetails() {
    this.loader = true;
    this.service.getPlansDetails().subscribe(
      resp => {
        console.log(resp);
        if (resp && resp['data']) {
          this.plansList = resp['data'];
        } else {
          this.plansList = [];
        }
        this.loader = false;
      },err => {
        this.loader = false;
        console.log(err);
      }
    );
  }

  getColor(name) {
    if (name) {
      if (name.toLowerCase() == 'basic') {
        return "#bfbbbbb3 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #bfbbbbb3";
      } else if (name.toLowerCase() == 'standard') {
        return "#F27526 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #F27526";
      } else {
        return "#2FA504 rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #2FA504";
      }
    } else {
      return "#fff rgba(0, 0, 0, 0) rgba(0, 0, 0, 0) #fff";
    }
  }
}
